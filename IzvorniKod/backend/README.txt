Dakle evo vam template za backend.

Stavio sam u paketima package-info.java datoteke u kojima su neke smjernice oko koda.

Ono što je potrebno da bi se ovo pokrenulo su:
	- Java 11, mada mislim da bi radilo i sa Java 8 al morate to pomejnit u pom.xml
	- postgres baza podataka i bilo bi lijepo neki program da možete manualno vidjet kaj je u bazi
I sad najbitnije, kad sve to imate onda odite u src/main/resources/application.properties.
	Tamo će te vidjet da sam na par property-a stavio xxxxxxxx, popunite to sa svojim podacima
	od baze. Prvi xxxxxxx ti je ime baze podataka, drugo je username i trece password(al ovo vam piše, al ok).
	Nakon što uspijete pokrenut(fingers crossed) onda dodajte taj application.properties u .gitignore u tom
	direktoriju tako da svakom ostanu njegovi podaci, da kad pullamo da ne pullamo nečiji tuđi podaci o bazi.

P.S.
	Dodajte i svoje smeće u .gitignore koji je u backend direktoriju.
	Ostavio sam vam ove dvije skripte za maven(mvnw, mvnw.cmd), one vam ovisno o os-u (mvnw za UNIX, mvnw.cmd za Win)
	mogu skinut maven i potebnu verziju jave, odnosno osposobit okolinu za pokretanje spring boot-a. Ako vam neće trebat
	lako ih maknemo kasnije. 

Ako neš zapne, znate di sam, zovite, vičite...
Sretno!