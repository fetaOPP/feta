package fer.feta.mediateka.mediatekabackend.purchase;

import fer.feta.mediateka.mediatekabackend.models.*;
import fer.feta.mediateka.mediatekabackend.repositories.ItemRepository;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import fer.feta.mediateka.mediatekabackend.repositories.TransactionRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * A test that checks {@link PurchasingService}.
 *
 * @author Mateo Imbrišak
 */

@SpringBootTest
@TestPropertySource(properties = {
        "spring.jpa.properties.hibernate.enable_lazy_load_no_trans=true"
})
class PurchasingServiceTest {

    private static final String USERNAME = "test";

    @Autowired
    private PurchasingService service;

    @Autowired
    private MediatekaUserRepository userRepo;

    @Autowired
    private ItemRepository itemRepo;

    @Autowired
    private TransactionRepository transactionRepo;

    private static MediatekaUser user;

    private Item item;

    @BeforeEach
    void beforeEach() {
        user = MediatekaUser.builder()
                .payPalMail("test@user.com")
                .role(RoleType.USER)
                .transactions(Set.of())
                .firstName("Test")
                .lastName("User")
                .username(USERNAME)
                .passwordHash(new BCryptPasswordEncoder().encode("1234"))
                .build();
    }

    @Test
    void testInvalidItemPurchase() {
        userRepo.save(user);

        PurchasingService.PurchaseInfo info = service.purchaseItem(PurchaseType.BUY, -1, user);
        assertFalse(info.isSuccess());

        userRepo.delete(user);
    }

    @Test
    void testValidPurchase() {
        item = Film.builder()
                .title("Test film")
                .fileUrl(null)
                .iconUrl(null)
                .available(true)
                .directors(Set.of())
                .producers(Set.of())
                .publishers(Set.of())
                .purchasePrice(BigDecimal.valueOf(10))
                .yearOfIssue(2112)
                .build();

        itemRepo.save(item);

        PurchasingService.PurchaseInfo info = service.purchaseItem(PurchaseType.BUY, item.getId(), user);
        assertTrue(info.isSuccess());

        MediatekaUser tempUser = userRepo.findByUsername(USERNAME);

        transactionRepo.deleteAll(tempUser.getTransactions());

        itemRepo.delete(item);
    }

    @Test
    void testDoublePurchase() {
        item = Film.builder()
                .title("Test film")
                .fileUrl(null)
                .iconUrl(null)
                .available(true)
                .directors(Set.of())
                .producers(Set.of())
                .publishers(Set.of())
                .purchasePrice(BigDecimal.valueOf(10))
                .yearOfIssue(2112)
                .build();

        itemRepo.save(item);

        PurchasingService.PurchaseInfo info = service.purchaseItem(PurchaseType.BUY, item.getId(), user);
        assertTrue(info.isSuccess());

        MediatekaUser tempUser = userRepo.findByUsername(USERNAME);

        info = service.purchaseItem(PurchaseType.BUY, item.getId(), tempUser);
        assertFalse(info.isSuccess());

        transactionRepo.deleteAll(tempUser.getTransactions());

        itemRepo.delete(item);
    }
}
