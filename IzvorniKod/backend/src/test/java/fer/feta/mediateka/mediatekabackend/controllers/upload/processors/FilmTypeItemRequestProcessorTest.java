package fer.feta.mediateka.mediatekabackend.controllers.upload.processors;

import fer.feta.mediateka.mediatekabackend.controllers.upload.ItemInfoDto;
import fer.feta.mediateka.mediatekabackend.models.*;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * A test that checks {@link ItemRequestProcessor}s.
 *
 * @author Mateo Imbrišak
 */

@SpringBootTest
class FilmTypeItemRequestProcessorTest {

    private static final String USERNAME = "test";

    @Autowired
    private FilmTypeItemRequestProcessor processor;

    private MediatekaUser user;

    @Autowired
    private MediatekaUserRepository userRepo;

    @BeforeEach
    void beforeEach() {
        user = MediatekaUser.builder()
                .payPalMail("test@user.com")
                .role(RoleType.COORDINATOR)
                .transactions(Set.of())
                .firstName("Test")
                .lastName("User")
                .username(USERNAME)
                .passwordHash(new BCryptPasswordEncoder().encode("1234"))
                .build();
    }

    @Test
    void testCoordinatorAdd() {
        userRepo.save(user);

        ItemInfoDto dto = new ItemInfoDto();
        dto.setDescription("desc");
        dto.setDirectors("Dir1, Dir2");
        dto.setItemType(ItemType.film);
        dto.setTitle("Test film");

        Film result = (Film) processor.process(dto, user);

        assertEquals("Test film", result.getTitle());
        assertEquals(2, result.getDirectors().size());

        userRepo.delete(user);
    }

    @Test
    void testCoordinatorAvailable() {
        userRepo.save(user);

        ItemInfoDto dto = new ItemInfoDto();
        dto.setDescription("desc");
        dto.setDirectors("Dir1, Dir2");
        dto.setItemType(ItemType.film);
        dto.setTitle("Test film");
        dto.setAvailable(true);

        Film result = (Film) processor.process(dto, user);

        assertFalse(result.isAvailable());

        userRepo.delete(user);
    }

    @Test
    void testAdminAvailable() {
        user.setRole(RoleType.ADMIN);

        userRepo.save(user);

        ItemInfoDto dto = new ItemInfoDto();
        dto.setDescription("desc");
        dto.setDirectors("Dir1, Dir2");
        dto.setItemType(ItemType.film);
        dto.setTitle("Test film");
        dto.setAvailable(true);

        Film result = (Film) processor.process(dto, user);

        assertTrue(result.isAvailable());

        userRepo.delete(user);
    }
}
