package fer.feta.mediateka.mediatekabackend.controllers.validation;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * A class containing tests for {@link EmailValidator}.
 *
 * @author Mateo Imbrišak
 */

@SpringBootTest
class EmailValidatorTest {

    private static final String TEST_USERNAME = "test";

    private static final String TEST_PASSWORD = "pass";

    private static final String TEST_TARGET = "payPalMail";

    private static final String EXPECTED_ERROR = "Email nije validnog formata";

    @Autowired
    private LocalValidatorFactoryBean validator;

    @Test
    public void testValidEmail() {
        MediatekaUserDto dto = createUser("test.user@user.com");
        Set<ConstraintViolation<MediatekaUserDto>> violations = validator.validateProperty(dto, TEST_TARGET);
        assertTrue(violations.isEmpty());

        dto = createUser("test-user_mail2112@mail.com.test");
        violations = validator.validateProperty(dto, TEST_TARGET);
        assertTrue(violations.isEmpty());

        dto = createUser("vAlIdleTTers@xyz4.hr");
        violations = validator.validateProperty(dto, TEST_TARGET);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void testMissingAt() {
        MediatekaUserDto dto = createUser("fail.test.com");
        Set<ConstraintViolation<MediatekaUserDto>> violations = validator.validateProperty(dto, TEST_TARGET);
        assertFalse(violations.isEmpty());
        assertEquals(1, violations.size());

        for (ConstraintViolation<MediatekaUserDto> violation : violations) {
            String message = violation.getMessage();

            assertEquals(EXPECTED_ERROR, message);
        }
    }

    @Test
    public void testInvalidDomainName() {
        MediatekaUserDto dto = createUser("fail@.com");
        Set<ConstraintViolation<MediatekaUserDto>> violations = validator.validateProperty(dto, TEST_TARGET);
        assertFalse(violations.isEmpty());
        assertEquals(1, violations.size());

        for (ConstraintViolation<MediatekaUserDto> violation : violations) {
            String message = violation.getMessage();

            assertEquals(EXPECTED_ERROR, message);
        }

        dto = createUser("fail@domain][com");
        violations = validator.validateProperty(dto, TEST_TARGET);
        assertFalse(violations.isEmpty());
        assertEquals(1, violations.size());

        for (ConstraintViolation<MediatekaUserDto> violation : violations) {
            String message = violation.getMessage();

            assertEquals(EXPECTED_ERROR, message);
        }
    }

    /**
     * Used to create a {@link MediatekaUserDto} to be tested.
     *
     * @param email used for testing.
     *
     * @return a new {@link MediatekaUserDto} with default parameters and given {@code email}.
     */
    private MediatekaUserDto createUser(String email) {
        MediatekaUserDto dto = new MediatekaUserDto();

        dto.setUsername(TEST_USERNAME);
        dto.setPayPalMail(email);
        dto.setPassword(TEST_PASSWORD);
        dto.setConfirmPassword(TEST_PASSWORD);

        return dto;
    }
}
