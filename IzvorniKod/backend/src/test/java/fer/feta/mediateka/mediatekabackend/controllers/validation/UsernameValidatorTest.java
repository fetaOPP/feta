package fer.feta.mediateka.mediatekabackend.controllers.validation;

import fer.feta.mediateka.mediatekabackend.controllers.RegistrationController;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * A test used to check {@link UsernameValidator}.
 *
 * @author Mateo Imbrišak
 */

@SpringBootTest
class UsernameValidatorTest {

    private static final String TEST_PASSWORD = "pass";

    private static final String  TEST_EMAIL = "test@user.com";

    private static final String TEST_TARGET =  "username";

    @Autowired
    private LocalValidatorFactoryBean validator;

    @Autowired
    private RegistrationController registration;

    @Autowired
    private MediatekaUserRepository userRepo;

    @Test
    void testNewUsername() {
        MediatekaUserDto dto = createUser("testuser");
        Set<ConstraintViolation<MediatekaUserDto>> violations = validator.validateProperty(dto, TEST_TARGET);
        assertTrue(violations.isEmpty());
    }

    @Test
    void testExistingName() {
        MediatekaUserDto dto = createUser("existing");
        Set<ConstraintViolation<MediatekaUserDto>> violations = validator.validateProperty(dto, TEST_TARGET);
        assertTrue(violations.isEmpty());

        violations = validator.validateProperty(dto, TEST_TARGET);
        assertTrue(violations.isEmpty());

        registration.registerUser(dto);

        violations = validator.validateProperty(dto, TEST_TARGET);
        assertFalse(violations.isEmpty());

        userRepo.delete(userRepo.findByUsername("existing"));
    }

    /**
     * Used to create a {@link MediatekaUserDto} to be tested.
     *
     * @param username used for testing.
     *
     * @return a new {@link MediatekaUserDto} with default parameters and given {@code email}.
     */
    private MediatekaUserDto createUser(String username) {
        MediatekaUserDto dto = new MediatekaUserDto();

        dto.setUsername(username);
        dto.setPayPalMail(TEST_EMAIL);
        dto.setPassword(TEST_PASSWORD);
        dto.setConfirmPassword(TEST_PASSWORD);

        return dto;
    }

}
