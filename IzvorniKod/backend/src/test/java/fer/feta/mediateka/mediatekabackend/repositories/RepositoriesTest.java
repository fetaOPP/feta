package fer.feta.mediateka.mediatekabackend.repositories;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.TestPropertySource;

import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate"
})
class RepositoriesTest {

	@Autowired
	private ItemRepository itemRepo;
	@Autowired
	private BookRepository bookRepo;
	@Autowired
	private PublisherRepository publisherRepo;

	@Test
	void testRepositoryIsNotNull() {
		assertNotNull(itemRepo);
		assertNotNull(bookRepo);
		assertNotNull(publisherRepo);
	}

	@Test
	void testAllRepositoriesHaveSomeValues() {
		haveSomeValue(itemRepo);
		haveSomeValue(bookRepo);
		haveSomeValue(publisherRepo);
	}

	private <T, ID>void haveSomeValue(CrudRepository<T, ID> repo) {
		assertNotEquals(0, StreamSupport.stream(repo.findAll().spliterator(), false).count());
	}

}
