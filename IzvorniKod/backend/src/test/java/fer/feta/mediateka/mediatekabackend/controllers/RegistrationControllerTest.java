package fer.feta.mediateka.mediatekabackend.controllers;

import fer.feta.mediateka.mediatekabackend.controllers.validation.MediatekaUserDto;
import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

/**
 * A test used to test valid user registration.
 *
 * @author Mateo Imbrišak
 */

@SpringBootTest
class RegistrationControllerTest {

    private static final String TEST_USERNAME = "test";

    private static final String TEST_PASSWORD = "pass";

    private static final String  TEST_EMAIL = "test@user.com";

    private static MediatekaUserDto dto;

    @Autowired
    private RegistrationController registration;

    @Autowired
    private MediatekaUserRepository userRepo;

    @BeforeAll
    static void beforeAll() {
        dto = new MediatekaUserDto();

        dto.setUsername(TEST_USERNAME);
        dto.setPayPalMail(TEST_EMAIL);
        dto.setPassword(TEST_PASSWORD);
        dto.setConfirmPassword(TEST_PASSWORD);
    }

    @Test
    void testValidRegistration() {
        ResponseEntity<MediatekaUser> response = registration.registerUser(dto);
        assertEquals(200, response.getStatusCodeValue());

        assertNotNull(userRepo.findByUsername(TEST_USERNAME));

        userRepo.delete(userRepo.findByUsername(TEST_USERNAME));
    }
}
