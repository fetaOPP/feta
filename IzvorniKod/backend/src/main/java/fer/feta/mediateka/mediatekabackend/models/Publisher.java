package fer.feta.mediateka.mediatekabackend.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * <pre>
 * This class encapsulates publisher persistence entity.
 * </pre>
 * 
 * @author hrvoje
 *
 */

@Builder
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Publisher {
	
	@Id @GeneratedValue
	private long id;
	
	@Column(length = ModelsConstants.PUBLISHER_NAME_MAX_LENGTH)
	private String name;
}
