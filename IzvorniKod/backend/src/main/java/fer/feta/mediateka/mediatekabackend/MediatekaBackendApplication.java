package fer.feta.mediateka.mediatekabackend;

import fer.feta.mediateka.mediatekabackend.resources.GCSInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * <pre>
 * This is the main class of the application which has has the main entry point.
 * </pre>
 * 
 * @author hrvoje
 *
 */

@SpringBootApplication // Activates all Spring Boot functionalities
@Import(value = {DatabaseAdder.class, GCSInitializer.class}) // Loads DatabaseAdder class when loading MediatekaBackendApplication class
@PropertySource("classpath:security.properties")
public class MediatekaBackendApplication {

	/**
	 * Main entry point of application
	 * @param args terminal arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(MediatekaBackendApplication.class, args);
	}
	
}
