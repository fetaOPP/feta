package fer.feta.mediateka.mediatekabackend.models;

import fer.feta.mediateka.mediatekabackend.models.generator.ItemTypeGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Set;

/**
 * <pre>
 * This class represents one "top of the tree" persistence entity. This entity has a corresponding table
 * in the database but its primarily intent is to be inherited.
 * </pre>
 *
 * @author hrvoje
 *
 */

@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Item {

	@Id @GeneratedValue
	private long id;

	private String title;

	@OneToMany(cascade = CascadeType.ALL)
	private Set<Publisher> publishers;

	@GeneratorType(type = ItemTypeGenerator.class, when = GenerationTime.INSERT)
	private ItemType type;
	
	private int yearOfIssue;

	@Column(length = ModelsConstants.ITEM_DESCRIPTION_MAX_LENGTH)
	private String description;

	@Column(precision = ModelsConstants.ITEM_PURCHASE_PRICE_PRECISION,
			scale = ModelsConstants.ITEM_PURCHASE_PRICE_SCALE)
	private BigDecimal purchasePrice;

	private boolean available;

	private URL iconUrl;

	private URL fileUrl;
}
