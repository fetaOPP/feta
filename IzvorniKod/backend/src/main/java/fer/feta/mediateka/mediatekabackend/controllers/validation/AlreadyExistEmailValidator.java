package fer.feta.mediateka.mediatekabackend.controllers.validation;

import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * <pre>
 * Class validates if supplied email already exists in database or not. It returns {@code true} if such email does not exist in database,
 * otherwise {@code false}
 * </pre>
 * 
 * @author hrvoje
 *
 */
@Component
public class AlreadyExistEmailValidator implements ConstraintValidator<AlreadyExistEmail, String> {
	
	/**
	 * Reference to user repository
	 */
	@Autowired
	private MediatekaUserRepository userRepo; 
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return userRepo.findBypayPalMail(value) == null;
	}

}
