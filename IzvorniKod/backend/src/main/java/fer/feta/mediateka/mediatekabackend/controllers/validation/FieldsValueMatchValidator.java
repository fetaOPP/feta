package fer.feta.mediateka.mediatekabackend.controllers.validation;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * <pre>
 * Class that validates if password and confirmPassword are the same.
 * </pre>
 * 
 * @author hrvoje
 *
 */
public class FieldsValueMatchValidator implements ConstraintValidator<FieldsValueMatch, Object> {

	private String field;
    private String fieldMatch;
 
    @Override
    public void initialize(FieldsValueMatch constraintAnnotation) {
        this.field = constraintAnnotation.field();
        this.fieldMatch = constraintAnnotation.fieldMatch();
    }
	
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		String fieldValue = (String) new BeanWrapperImpl(value)
		          .getPropertyValue(field);
		String fieldMatchValue = (String) new BeanWrapperImpl(value)
		          .getPropertyValue(fieldMatch);
		return fieldValue != null && fieldValue.equals(fieldMatchValue);
	}

}
