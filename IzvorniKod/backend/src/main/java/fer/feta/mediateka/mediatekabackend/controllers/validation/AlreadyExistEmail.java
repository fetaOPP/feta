package fer.feta.mediateka.mediatekabackend.controllers.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <pre>
 * Annotation that is used to annotate a field that stores email as a string. It uses {@link AlreadyExistEmailValidator}
 * to check if there is already such email present. If such email is present error is thrown with a custom error message.
 * </pre>
 * 
 * @author hrvoje
 *
 */
@Target({TYPE, FIELD, ANNOTATION_TYPE}) 
@Retention(RUNTIME)
@Constraint(validatedBy = AlreadyExistEmailValidator.class)
@Documented
public @interface AlreadyExistEmail {   
    String message() default "Email already exists";
    Class<?>[] groups() default {}; 
    Class<? extends Payload>[] payload() default {};
}

