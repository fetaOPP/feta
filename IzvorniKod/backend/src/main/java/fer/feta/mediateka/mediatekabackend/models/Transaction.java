package fer.feta.mediateka.mediatekabackend.models;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.IOException;
import java.util.Date;

/**
 * A class that represents a single transaction made by a {@link MediatekaUser}.
 *
 * @author Mateo Imbrišak
 */

@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Transaction {

    @Id @GeneratedValue
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonSerialize(using = CustomJsonSerializer.class)
    private MediatekaUser user;

    @ManyToOne
    private Item item;

    private Date timeOfTransaction;

    private Date validUntil;

    private TransactionType type;

    public boolean isValid() {
        return type == TransactionType.PURCHASE;
    }

    private static class CustomJsonSerializer extends JsonSerializer {

        @Override
        public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", ((MediatekaUser)o).getId());
            jsonGenerator.writeEndObject();
        }
    }
}
