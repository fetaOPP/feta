package fer.feta.mediateka.mediatekabackend.repositories;

import fer.feta.mediateka.mediatekabackend.models.Song;
import org.springframework.data.repository.CrudRepository;

public interface SongRepository extends CrudRepository<Song, Long> {
    Song findByTitle(String title);
}
