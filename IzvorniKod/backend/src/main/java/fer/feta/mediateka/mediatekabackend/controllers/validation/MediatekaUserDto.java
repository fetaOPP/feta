package fer.feta.mediateka.mediatekabackend.controllers.validation;

import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.models.RoleType;
import lombok.Data;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

/**
 * <pre>
 * Class is made to be data access object for {@link MediatekaUser} class. Data is supplied by
 * web client and it is able to be transformed to {@link MediatekaUser} by method {@link MediatekaUserDto#getMediatekaUser()}.
 * </pre>
 *
 * @author hrvoje
 *
 */
@Component
@Data
@FieldsValueMatch(
		field = "password",
		fieldMatch = "confirmPassword",
		message = "Lozinke moraju biti jednake")
public class MediatekaUserDto {

	@ValidEmail(message = "Email nije validnog formata")
	@AlreadyExistEmail(message = "Email već postoji")
	@NotEmpty(message = "Email ne smije biti prazan")
	private String payPalMail;

	@ValidUsername(message = "Korsiničko ime već postoji")
	@NotEmpty(message = "Korisničko ime ne smije biti prazno")
	private String username;

	@ValidName(message = "Ime nije valjano")
	private String firstName;

	@ValidName(message = "Prezime smije sadržati samo slova")
	private String lastName;

	@NotEmpty(message = "Lozinka ne može biti prazna")
	private String password;

	@NotEmpty(message = "Potvrda lozinke ne smije biti prazna")
	private String confirmPassword;

	/**
	 * <pre>
	 * Method transforms this object to {@link MediatekaUser} object.
	 * </pre>
	 * @return {@link MediatekaUser} created from this object.
	 */
	public MediatekaUser getMediatekaUser() {
		return MediatekaUser
				.builder()
					.payPalMail(payPalMail)
					.username(username)
					.firstName(firstName)
					.lastName(lastName)
					.role(RoleType.USER)
					.passwordHash(new BCryptPasswordEncoder().encode(password))
					.transactions(Set.of())
				.build();
	}

}
