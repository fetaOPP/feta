package fer.feta.mediateka.mediatekabackend.models;


/**
 * <pre>
 * Enum that contains all {@link Item} types.
 * </pre>
 * @author hrvoje
 *
 */
public enum ItemType {

	song,
	film,
	book,
	magazine
}
