package fer.feta.mediateka.mediatekabackend.controllers;

import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import fer.feta.mediateka.mediatekabackend.resources.ResourceStore;
import lombok.Builder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/resource")
public class ResourceController {

	@Autowired
	private ResourceStore store;

	@Autowired
	private MediatekaUserRepository mediatekaUserRepository;

	/**
	 * <pre>
	 *     Method takes in a type of the item that is uploaded, that type must be some MIME type, like:
	 *     <ul>
	 *     	   <li>video/mp4</li>
	 *     	   <li>audio/mp3</li>
	 *     	   <li>application/pdf</li>
	 *     </ul>
	 *
	 *     With type parameter it takes in and the wanted name of the item, this does not mean that will
	 *     be the final name of the item. The final name will be returned in the response JSON body as attribute
	 *     "name".
	 *     {@Link HttpSession} argument is here only to set an item name to session storage, this name
	 *     will be deleted once the file was successfully uploaded with all its components that means
	 *     items file, items thumbnail and items info.
	 *
	 * 		Method returns {@Link ResponseDTO} that is data transfer object and its intent is to be serialize
	 * 		as JSON with two fields. First field is called "link" which value stores link to which user can upload
	 * 		the wanted file. Second filed is called "name", this field contains the final name of the item that is
	 * 		given by the application because the firstly given name by the user was already used with the other item.
	 *
	 * 		Link given by the response of this method can only be used for PUT requests within the 3 minutes and
	 * 		the request must contain the "Content-Type" header equal to the MIME type that was sent as the type
	 * 		argument to this method.
	 * </pre>
	 *
	 * @param type MIME type of the item that is uploaded
	 * @param name wanted name of the item that is uploaded, this does not mean that name will be the final name of
	 *             the item that is uploaded
	 * @param session {@Link HttpSession} that is used to store variables to session context
	 * @return {@Link ResponseDTO} that is serialized into JSON and contains fields "link" and "name"
	 */
	@GetMapping("/itemPutUrl")
	public ResponseDTO getItemPutUrl(@RequestParam String type, @RequestParam String name, HttpSession session) {
		while(store.containsItem(name)) {
			name = name+"0";
		}
		session.setAttribute("item", name);

		String link = store.getFilePutLink(name, 3, TimeUnit.MINUTES, type).toString();
		return ResponseDTO
				.builder()
					.link(link)
					.name(name)
				.build();
	}

	/**
	 * <pre>
	 * 		Method takes in the wanted name of the item from the user, this does not mean that this will be
	 * 		the final name of the item. Application will return the final name in the response body, this can
	 * 		happen because there can be already a thumbnail with such name stored in the database so the application
	 * 		will give the user another that is not already taken.
	 *
	 * 		{@Link HttpSession} is used to store parameters in the session context, in this method it only saves
	 * 		one parameter with the name of "thumbnail" and the value is set to be the final name of the thumbnail,
	 * 		this name can also be equal to the user given name but it's not guaranteed.
	 *
	 * 		Method returns {@Link ResponseDTO} object that is intended to be serialized to JSON that contains two
	 * 		fields, one called "link" that stores a link where user can upload the wanted thumbnail and the second
	 * 		named "name" which contains the final name of the thumbnail that is uploaded.
	 *
	 * 		A link that is returned by this method can only be used with PUT method for a limited time of 3 minutes,
	 * 		after that time the link is not usable anymore.
	 * </pre>
	 *
	 * @param name wanted name of the item given by the user, this does not mean that this name will be used for storing
	 *             the item it can change
	 * @param session {@Link HttpSession} is used for storing attributes in session context
	 * @return {@Link ResponseDTO} that is serialized into JSON with two fields "link" and "name"
	 */
	@GetMapping("/thumbnailPutUrl")
	public ResponseDTO getThumbnailPutLink(@RequestParam String name, HttpSession session) {
		while(store.containsItem(name)) {
			name = name+"0";
		}
		session.setAttribute("thumbnail", name);

		String link = store.getThumbnailPutLink(name, 3, TimeUnit.MINUTES).toString();
		return ResponseDTO
				.builder()
					.name(name)
					.link(link)
				.build();
	}

	/**
	 * <pre>
	 *     Method takes in the name of the file that it wants get a link to and the method returns
	 *     it as a string. This method also checks if current logged in user has privilege to make
	 *     a request to get the file with this name. If user has such privilege than the link is
	 *     returned otherwise there is no body sent back to user. Also link that the user gets will
	 *     be valid for only three minutes.
	 *
	 *     The link that's given to the user can only be used for making GET requests.
	 * </pre>
	 *
	 * @param name name of the file that user wants the get a link to
	 * @param principal currently logged in user
	 * @return link to the wanted file if user has a privilege to it nothing otherwise
	 */
	@GetMapping("/itemGetUrl")
	public ResponseEntity<String> getFileGetLink(@RequestParam String name, Principal principal) {
		MediatekaUser currentUser = mediatekaUserRepository.findByUsername(principal.getName());
		if(currentUser == null) return ResponseEntity.badRequest().build();

		if(currentUser.getTransactions().stream()
				.filter(t -> t.getItem().getTitle().equals(name)).noneMatch(t -> !t.isValid())) return ResponseEntity.badRequest().build();

		return ResponseEntity.ok(store.getFileGetLink(name, 3, TimeUnit.MINUTES).toString());
	}
	
	@Builder
	@Data
	private static class ResponseDTO {
		private String link;
		private String name;
	}
	
}
