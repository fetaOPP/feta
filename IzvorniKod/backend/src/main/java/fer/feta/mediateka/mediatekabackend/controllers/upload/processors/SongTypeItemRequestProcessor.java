package fer.feta.mediateka.mediatekabackend.controllers.upload.processors;

import fer.feta.mediateka.mediatekabackend.controllers.upload.ItemInfoDto;
import fer.feta.mediateka.mediatekabackend.models.*;
import fer.feta.mediateka.mediatekabackend.repositories.AlbumRepository;
import fer.feta.mediateka.mediatekabackend.repositories.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class SongTypeItemRequestProcessor implements RequestProcessor {

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private ItemRequestProcessor itemPreprocessor;

    @Override
    public Item process(ItemInfoDto dto, MediatekaUser user) {

        // Album
        Album albumLinked = dto.getAlbum()==null ?
                null :
                albumRepository.findByTitle(dto.getAlbum());

        // All artists
        Set<Artist> allArtists = StreamSupport
                .stream(artistRepository.findAll().spliterator(),false)
                .collect(Collectors.toSet());

        // Artists
        Set<Artist> artistsNew = dto.getArtists()==null ?
                null : Set.of(dto.getArtists().split(","))
                .stream()
                .map(str->Artist.builder().name(str).build())
                .collect(Collectors.toSet());

        Set<Artist> artistLinked = dto.getArtistsIds()==null ?
                null :
                allArtists.stream()
                        .filter(a -> dto.getArtistsIds().contains(a.getId()))
                        .collect(Collectors.toSet());

        // Songwriters
        Set<Artist> songwritersNew = dto.getSongwriters()==null ?
                null : Set.of(dto.getSongwriters().split(","))
                .stream()
                .map(str->Artist.builder().name(str).build())
                .collect(Collectors.toSet());

        Set<Artist> songwritersLinked = dto.getSongwritersIds()==null ?
                null :
                allArtists.stream()
                        .filter(a -> dto.getSongwritersIds().contains(a.getId()))
                        .collect(Collectors.toSet());

        // Producers
        Set<Artist> producersNew = dto.getProducers()==null ?
                null : Set.of(dto.getProducers().split(","))
                .stream()
                .map(str->Artist.builder().name(str).build())
                .collect(Collectors.toSet());

        Set<Artist> producersLinked = dto.getProducersIds()==null ?
                null :
                allArtists.stream()
                        .filter(a -> dto.getProducersIds().contains(a.getId()))
                        .collect(Collectors.toSet());

        Album album = albumLinked == null ?
                Album.builder()
                .title(dto.getAlbum())
                .build() : albumLinked;

        Set<Artist> songwriters = new HashSet<>();
        if(songwritersNew != null) songwriters.addAll(songwritersNew);
        if(songwritersLinked != null) songwriters.addAll(songwritersLinked);

        Set<Artist> producers = new HashSet<>();
        if(producersNew != null) producers.addAll(producersNew);
        if(producersLinked != null) producers.addAll(producersLinked);

        Set<Artist> artists = new HashSet<>();
        if(artistsNew != null) artists.addAll(artistsNew);
        if(artistLinked != null) artists.addAll(artistLinked);

        Song newSong = Song
                .builder()
                    .trackNumber(dto.getTrackNumber())
                    .songwriters(songwriters)
                    .producers(producers)
                    .artists(artists)
                    .album(album)
                .build();

        return itemPreprocessor.processItem(dto,user,newSong);
    }
}
