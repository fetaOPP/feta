package fer.feta.mediateka.mediatekabackend.models;

/**
 * Describes all possible {@link Transaction} types.
 *
 * @author Mateo Imbrišak
 */

public enum TransactionType {

    /**
     * Item is permanently owned by the user.
     */
    PURCHASE,

    /**
     * Item is rented for a specific amount of time.
     */
    RENTAL
}
