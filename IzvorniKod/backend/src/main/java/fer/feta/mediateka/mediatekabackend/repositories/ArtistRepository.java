package fer.feta.mediateka.mediatekabackend.repositories;

import fer.feta.mediateka.mediatekabackend.models.Artist;
import org.springframework.data.repository.CrudRepository;

public interface ArtistRepository extends CrudRepository<Artist, Long> {}
