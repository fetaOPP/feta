package fer.feta.mediateka.mediatekabackend.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.Set;

/**
 * <pre>
 * This class encapsulates song persistence entity.
 * </pre>
 *
 * @author Mateo Imbrišak
 */

@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Song extends Item {

    private short trackNumber;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Artist> artists;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Artist> songwriters;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Artist> producers;

    @ManyToOne(cascade = CascadeType.ALL)
    private Album album;
}
