package fer.feta.mediateka.mediatekabackend;

import io.crnk.core.boot.CrnkBoot;
import io.crnk.core.queryspec.pagingspec.NumberSizePagingBehavior;
import io.crnk.spring.setup.boot.core.CrnkBootConfigurer;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 *     Class configures crnk framework.
 * </pre>
 */
@Configuration
public class CrnkConfig implements CrnkBootConfigurer {

    @Override
    public void configure(CrnkBoot crnkBoot) {
        crnkBoot.addModule(NumberSizePagingBehavior.createModule());
    }
}
