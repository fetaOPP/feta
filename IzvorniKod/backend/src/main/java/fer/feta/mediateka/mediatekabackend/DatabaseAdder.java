package fer.feta.mediateka.mediatekabackend;

import fer.feta.mediateka.mediatekabackend.models.*;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

/**
 * <pre>
 * This class only exist for adding entities to database.
 * </pre>
 *
 * @author hrvoje
 *
 */
@Configuration
public class DatabaseAdder {

	@PersistenceContext // Injects Entity Manager
	private EntityManager em;

	/**
	 * <pre>
	 * This method is run when application has loaded all beans. Method basically initializes database with
	 * some values. To see database properties see src/main/resources/application.properties
	 * </pre>
	 */
	@Transactional // It means that this method needs to be run in one transaction.
	@EventListener(classes = ApplicationReadyEvent.class) // This annotation indicates Spring Framework to invoke this method when application loaded all beans.
	public void onApplicationBooted() {
		Artist a = Artist.builder()
				.name("Ivan Mažuranić")
				.build();

		Publisher p = Publisher.builder()
				.name("Školska knjiga")
				.build();

		URL bookThumbnail = null;
		URL songThumbnail = null;
		URL songLateralusThumbnail = null;
		URL filmThumbnail = null;
		URL modraLastaThumbnail = null;
		try {
			bookThumbnail = new URL("https://storage.googleapis.com/mediateka-bucket/smrtSmailAge.png");
			songThumbnail = new URL("https://storage.googleapis.com/mediateka-bucket/meddle.png");
			songLateralusThumbnail = new URL("https://storage.googleapis.com/mediateka-bucket/Lateralus.png");
			filmThumbnail = new URL("https://storage.googleapis.com/mediateka-bucket/mean-street.png");
			modraLastaThumbnail = new URL("https://storage.googleapis.com/mediateka-bucket/lasta_logo.png");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		Book b = Book.builder()
				.description("Neki car")
				.fileUrl(bookThumbnail)
				.iconUrl(bookThumbnail)
				.title("Smrt Smail-age Čengića")
				.purchasePrice(BigDecimal.valueOf(50))
				.yearOfIssue(1789)
				.publishers(Set.of(p))
				.authors(Set.of(a))
				.build();

		Artist pf = Artist.builder()
				.name("Pink Floyd")
				.build();

		Album album = Album.builder()
				.title("Meddle")
				.build();

		Artist dg = Artist.builder()
				.name("David Gilmour")
				.build();

		Artist rw = Artist.builder()
				.name("Roger Waters")
				.build();

		Song song1 = Song.builder()
				.title("Echoes")
				.fileUrl(songThumbnail)
				.iconUrl(songThumbnail)
				.description("")
				.purchasePrice(BigDecimal.valueOf(1))
				.yearOfIssue(1971)
				.artists(Set.of(pf))
				.album(album)
				.trackNumber((short) 6)
				.build();

		Song song2 = Song.builder()
				.title("Fearless")
				.fileUrl(songThumbnail)
				.iconUrl(songThumbnail)
				.description("")
				.purchasePrice(BigDecimal.valueOf(1))
				.yearOfIssue(1971)
				.artists(Set.of(pf))
				.album(album)
				.trackNumber((short) 3)
				.build();

		song1.setSongwriters(Set.of(dg, rw));
		song1.setProducers(Set.of(dg, rw));

		song2.setSongwriters(Set.of(dg, rw));
		song2.setProducers(Set.of(dg, rw));

		Artist tool = Artist.builder()
				.name("TOOL")
				.build();

		Album lateralus = Album.builder()
				.title("Lateralus")
				.build();

		Song song3 = Song.builder()
				.title("Lateralus")
				.fileUrl(songLateralusThumbnail)
				.iconUrl(songLateralusThumbnail)
				.description("Spiral out")
				.purchasePrice(BigDecimal.valueOf(1))
				.yearOfIssue(2001)
				.artists(Set.of(tool))
				.album(lateralus)
				.trackNumber((short) 9)
				.build();

		Song song4 = Song.builder()
				.title("Parabola")
				.fileUrl(songLateralusThumbnail)
				.iconUrl(songLateralusThumbnail)
				.description("")
				.purchasePrice(BigDecimal.valueOf(1))
				.yearOfIssue(2001)
				.artists(Set.of(tool))
				.album(lateralus)
				.trackNumber((short) 7)
				.build();

		Artist mjk = Artist.builder()
				.name("Maynard James Keenan")
				.build();

		song3.setSongwriters(Set.of(mjk));
		song4.setSongwriters(Set.of(mjk));

		Artist db = Artist.builder()
				.name("David Bottrill")
				.build();

		song3.setProducers(Set.of(db));
		song4.setProducers(Set.of(db));

		Film ms = Film.builder()
				.title("Mean Streets")
				.fileUrl(filmThumbnail)
				.iconUrl(filmThumbnail)
				.purchasePrice(BigDecimal.valueOf(10))
				.yearOfIssue(1973)
				.build();

		Artist dir = Artist.builder()
				.name("Martin Scorsese")
				.build();

		Artist jtt = Artist.builder()
				.name("Jonathan T. Taplin")
				.build();

		MediatekaUser user = MediatekaUser.builder()
								.payPalMail("hrvoje.bolesic@fer.hr")
								.role(RoleType.ADMIN)
								.transactions(Set.of())
								.username("hrco159753")
								.passwordHash(encoder().encode("1234"))
							.build();

		MediatekaUser coordinatorUser =
				MediatekaUser.builder()
						.payPalMail("coordinator@fer.hr")
						.role(RoleType.COORDINATOR)
						.transactions(Set.of())
						.username("coordinator")
						.passwordHash(encoder().encode("1234"))
				.build();

		MediatekaUser normalUser =
				MediatekaUser.builder()
					.payPalMail("user@fer.hr")
					.role(RoleType.USER)
					.transactions(Set.of())
					.firstName("Normal")
					.lastName("User")
					.username("user")
					.passwordHash(encoder().encode("1234"))
				.build();

		Magazine magazine =
				Magazine.builder()
					.available(true)
					.description("Novi moderni časopis za sve uzraste.")
					.title("Modra lasta")
					.fileUrl(modraLastaThumbnail)
					.iconUrl(modraLastaThumbnail)
					.issueNumber(1)
					.monthOfIssue(12)
					.purchasePrice(BigDecimal.TEN)
					.publishers(Set.of())
				.build();

		em.persist(user);
		em.persist(coordinatorUser);
		em.persist(normalUser);

		ms.setDirectors(Set.of(dir));
		ms.setProducers(Set.of(jtt));

		em.persist(a);
		em.persist(p);
		em.persist(b);

		em.persist(pf);
		em.persist(dg);
		em.persist(album);
		em.persist(song1);
		em.persist(song2);

		em.persist(tool);
		em.persist(mjk);
		em.persist(db);
		em.persist(lateralus);
		em.persist(song3);
		em.persist(song4);

		em.persist(dir);
		em.persist(jtt);
		em.persist(ms);

		em.persist(magazine);
	}

	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
}

