package fer.feta.mediateka.mediatekabackend.purchase;

/**
 * <pre>
 *     This enumeration contains all purchase types available in the application.
 * </pre>
 */
public enum PurchaseType {
    DAY, TWO_DAY, WEEK, MONTH, BUY
}
