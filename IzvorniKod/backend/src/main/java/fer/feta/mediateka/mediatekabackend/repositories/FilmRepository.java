package fer.feta.mediateka.mediatekabackend.repositories;

import fer.feta.mediateka.mediatekabackend.models.Film;
import org.springframework.data.repository.CrudRepository;

public interface FilmRepository extends CrudRepository<Film, Long> {}
