package fer.feta.mediateka.mediatekabackend.resources;

import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * <pre>
 * This class initializes all objects that are needed for communication to google cloud storage.
 * Please don't touch :).
 * </pre>
 * @author hrvoje
 *
 */
@Configuration
public class GCSInitializer {
	
	@Value("classpath:gcs-key.json")
	private Resource gcsKey;
	
	@Value("${gcs.bucketName}")
	private String bucketName;
	
	@Bean
	public Bucket getBucket() throws IOException {
		return getStorage().get(bucketName);
	}
	
	private Credentials getCredentials() throws IOException {
		return GoogleCredentials.fromStream(gcsKey.getInputStream());
	}
	
	private Storage getStorage() throws IOException {
		return StorageOptions.newBuilder().setCredentials(getCredentials())
				  .setProjectId("baeldung-cloud-tutorial").build().getService();
	} 
	
}
