package fer.feta.mediateka.mediatekabackend.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.Set;

/**
 * <pre>
 * This class encapsulates book persistence entity.
 * </pre>
 *
 * @author hrvoje
 *
 */
@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Book extends Item {

	@ManyToMany(cascade = CascadeType.ALL)
	private Set<Artist> authors;

}
