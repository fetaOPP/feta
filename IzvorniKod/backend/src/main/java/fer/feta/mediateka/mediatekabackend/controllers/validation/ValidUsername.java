package fer.feta.mediateka.mediatekabackend.controllers.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <pre>
 * Annotation that is used to annotate a field that keeps the username as a string. It uses {@link UsernameValidator}
 * to see if there exists the same username.
 * </pre>
 * 
 * @author hrvoje
 *
 */
@Target({TYPE, FIELD, ANNOTATION_TYPE}) 
@Retention(RUNTIME)
@Constraint(validatedBy = UsernameValidator.class)
@Documented
public @interface ValidUsername {   
    String message() default "Username already exists";
    Class<?>[] groups() default {}; 
    Class<? extends Payload>[] payload() default {};
}
