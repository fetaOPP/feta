/**
 * <pre>
 * This package stores all persistence entities for the mediateka application.
 * 
 * For all information on generic annotation that are used in this package see 
 * {@code fer.feta.mediateka.mediatekabackend.models.Item} class.
 * </pre>
 */
package fer.feta.mediateka.mediatekabackend.models;