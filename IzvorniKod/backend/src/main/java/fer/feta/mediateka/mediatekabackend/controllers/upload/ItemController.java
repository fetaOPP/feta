package fer.feta.mediateka.mediatekabackend.controllers.upload;

import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import fer.feta.mediateka.mediatekabackend.models.Item;
import fer.feta.mediateka.mediatekabackend.repositories.ItemRepository;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.security.Principal;
import java.util.Arrays;
import java.util.Optional;

/**
 * <pre>
 *     This class encapsulates the controller that is used for uploading item info.
 * </pre>
 */
@RestController
@RequestMapping("/resource")
public class ItemController {

	@Autowired
	private Bucket bucket;

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private MediatekaUserRepository mediatekaUserRepository;

	@Autowired
	private ItemFactory itemFactory;

	@PostMapping("/uploadItem")
	public ResponseEntity<Item> uploadItemInfo(Principal principal, HttpSession session, @RequestBody ItemInfoDto itemInfoDto, HttpServletRequest servletRequest) throws IOException {

		if(itemInfoDto.getId() != null && itemRepository.findById(itemInfoDto.getId()).isEmpty()) return ResponseEntity.notFound().build();

		URL thumbnailUrl = null, fileGetUrl = null;

		if(itemInfoDto.getId() == null) {
			String item = (String) session.getAttribute("item");
			String thumbnail = (String) session.getAttribute("thumbnail");

			if (item == null || thumbnail == null) {
				return ResponseEntity.badRequest().build();
			}

			Blob thumbnailBlob = bucket.get(thumbnail), itemBlob = bucket.get(item);
			if (thumbnailBlob == null || itemBlob == null) {
				return ResponseEntity.badRequest().build();
			}

			// Make thumbnail publicly available
			bucket
					.getStorage()
					.createAcl(thumbnailBlob.getBlobId(), Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER));

			// Get publicly available URL for thumbnail
			thumbnailUrl = new URL("https://storage.googleapis.com/" + bucket.getName() + "/" + thumbnail);

			String baseUrl = String.format("%s://%s:%s",
					servletRequest.getScheme(),
					servletRequest.getServerName(),
					servletRequest.getServerName().matches("localhost*") ? servletRequest.getServerPort() : "");

			// Get link to endpoint that gets signed url of item
			fileGetUrl = new URL(baseUrl + "/resource/itemGetUrl?name=" + item);

		}

		// Create new item
		Item newItem = itemFactory.getItem(itemInfoDto, mediatekaUserRepository.findByUsername(principal.getName()));

		if(itemInfoDto.getId() != null) {
			// If item already exists
			Item existingItem = itemRepository.findById(itemInfoDto.getId()).get();
			newItem.setId(existingItem.getId());
			newItem.setFileUrl(existingItem.getFileUrl());
			newItem.setIconUrl(existingItem.getIconUrl());
		} else {
			// Set url-s to thumbnail and file
			newItem.setIconUrl(thumbnailUrl);
			newItem.setFileUrl(fileGetUrl);
		}

		// Save new item
		itemRepository.save(newItem);

		return ResponseEntity.ok(newItem);
	}

}
