package fer.feta.mediateka.mediatekabackend.controllers.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * A class that checks whether first name and last name contain any illegal characters.
 *
 * @author Mateo Imbrišak
 */
public class NameValidator implements ConstraintValidator<ValidName, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return validateName(s);
    }

    /**
     * Checks that the given {@code name} contains only letters.
     *
     * @param name being checked.
     * @return {@code true} if the given {@code name} contains only
     *          letters, otherwise {@code false}.
     */
    private boolean validateName(String name) {
        if(name == null) return true;
        char[] charArray = name.toCharArray();

        for (char c : charArray) {
            if (!Character.isAlphabetic(c)) {
                return false;
            }
        }

        return true;
    }
}
