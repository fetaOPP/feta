package fer.feta.mediateka.mediatekabackend.repositories;

import fer.feta.mediateka.mediatekabackend.models.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {}
