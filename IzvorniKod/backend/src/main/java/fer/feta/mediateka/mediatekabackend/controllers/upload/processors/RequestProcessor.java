package fer.feta.mediateka.mediatekabackend.controllers.upload.processors;

import fer.feta.mediateka.mediatekabackend.controllers.upload.ItemInfoDto;
import fer.feta.mediateka.mediatekabackend.models.Item;
import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;

/**
 * <pre>
 *     This interface encapsulates one request processor that processes one client request body(e.g. JSON body)
 *     and returns an {@link fer.feta.mediateka.mediatekabackend.models.Item} reference.
 * </pre>
 */
public interface RequestProcessor {

    /**
     * <pre>
     *     Method takes int {@link ItemInfoDto} object and {@link MediatekaUser} from which
     *     the request was given and returns an {@Item} object dependent on that two parameters.
     * </pre>
     *
     * @param dto data transfer object that encapsulates JSON body from the user request
     * @param user user that made the request
     * @return item that user wants to create
     */
    Item process(ItemInfoDto dto, MediatekaUser user);
}
