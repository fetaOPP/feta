package fer.feta.mediateka.mediatekabackend.controllers.upload.processors;

import fer.feta.mediateka.mediatekabackend.controllers.upload.ItemInfoDto;
import fer.feta.mediateka.mediatekabackend.models.Artist;
import fer.feta.mediateka.mediatekabackend.models.Film;
import fer.feta.mediateka.mediatekabackend.models.Item;
import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.repositories.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class FilmTypeItemRequestProcessor implements RequestProcessor {

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private ItemRequestProcessor itemPreprocessor;

    @Override
    public Item process(ItemInfoDto dto, MediatekaUser user) {
        Set<Artist> directorsNew = dto.getDirectors()==null ?
                null :
                Set.of(dto.getDirectors().split(","))
                        .stream().map(str->Artist.builder().name(str).build()).collect(Collectors.toSet());
        Set<Artist> directorsLinked = dto.getDirectorsIds()==null ?
                null :
                StreamSupport
                        .stream(artistRepository.findAllById(dto.getDirectorsIds()).spliterator(), false)
                        .collect(Collectors.toSet());

        Set<Artist> producersNew = dto.getProducers()==null ?
                null :
                Set.of(dto.getProducers().split(","))
                        .stream().map(str->Artist.builder().name(str).build()).collect(Collectors.toSet());
        Set<Artist> producersLinked = dto.getProducersIds()==null ?
                null :
                StreamSupport
                        .stream(artistRepository.findAllById(dto.getProducersIds()).spliterator(), false)
                        .collect(Collectors.toSet());

        Set<Artist> producers = new HashSet<>();
        if(producersNew != null) producers.addAll(producersNew);
        if(producersLinked != null) producers.addAll(producersLinked);

        Set<Artist> directors = new HashSet<>();
        if(directorsNew != null) directors.addAll(directorsNew);
        if(directorsLinked != null) directors.addAll(directorsLinked);

        Film newFilm = Film
                .builder()
                    .directors(directors)
                    .producers(producers)
                .build();

        return itemPreprocessor.processItem(dto,user, newFilm);
    }
}
