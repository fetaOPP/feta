/**
 * <pre>
 * This package stores all needed repositories interfaces that are need for mediateka application.
 * 
 * For all details of this package classes and interfaces see {@code fer.feta.mediateka.mediatekabackend.repositories.ItemRepository} interface. 
 * </pre>
 */
package fer.feta.mediateka.mediatekabackend.repositories;