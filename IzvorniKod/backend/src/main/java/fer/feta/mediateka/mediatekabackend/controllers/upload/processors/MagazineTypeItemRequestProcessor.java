package fer.feta.mediateka.mediatekabackend.controllers.upload.processors;

import fer.feta.mediateka.mediatekabackend.controllers.upload.ItemInfoDto;
import fer.feta.mediateka.mediatekabackend.models.Item;
import fer.feta.mediateka.mediatekabackend.models.Magazine;
import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MagazineTypeItemRequestProcessor implements RequestProcessor {

    @Autowired
    private ItemRequestProcessor itemPreprocessor;

    @Override
    public Item process(ItemInfoDto dto, MediatekaUser user) {

            Magazine newBook = Magazine
                    .builder()
                        .monthOfIssue(dto.getMonthOfIssue())
                        .issueNumber(dto.getIssueNumber())
                    .build();

            return itemPreprocessor.processItem(dto,user,newBook);
    }

}
