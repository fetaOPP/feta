package fer.feta.mediateka.mediatekabackend.security;

import io.crnk.core.engine.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <pre>
 * Class encapsulates setup of basic http security settings and authentication management settings.
 * </pre>
 * 
 * @author hrvoje
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * Reference to custom implementation of {@link UserDetailsService}
	 */
	@Autowired
	private CustomUserDetailsService userService;

	@Value("${cors.hosts}")
	private List<String> corsHosts;

	@Value("${permitted.endpoints}")
	private String[] permittedEndpoints;

	@Value("${permitted.crnk.endpoints}")
	private String[] permittedCrnkEndpoints;

	@Value("${permitted.admin.endpoints}")
	private String[] permittedAdminEndpoints;

	@Value("${permitted.coordinator.endpoints}")
	private String[] permittedCoordinatorEndpoints;

	@Value("${permitted.user.endpoints}")
	private String[] permittedUserEndpoints;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().exceptionHandling();
		
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
		
		http.cors();

		http.logout().disable();

		http.authorizeRequests()
			.antMatchers(permittedEndpoints).permitAll()
			.antMatchers(HttpMethod.GET, permittedCrnkEndpoints).permitAll()
			.antMatchers(permittedAdminEndpoints).hasAuthority("ADMIN")
			.antMatchers(permittedCoordinatorEndpoints).hasAnyAuthority("COORDINATOR", "ADMIN")
			.antMatchers(permittedUserEndpoints).hasAnyAuthority("USER", "COORDINATOR", "ADMIN")
			.antMatchers("/**").authenticated();

		http.logout().logoutSuccessHandler(logoutSuccessHandler);
	}

	@Override
	@Bean
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	/**
	 * Object that handles successful logout. It just sends status 200 to the client.
	 */
	private static LogoutSuccessHandler logoutSuccessHandler = (request, response, authentication) -> response.setStatus(HttpStatus.OK_200);
	
	/**
	 * <pre>
	 * Method creates a bean that is used in cross origin resource sharing. It adds headers to any
	 * request that is mapped to any end point on the domain. The bean is a type of filter that 
	 * intercepts any request to the application and adds response headers.
	 * </pre>
	 * 
	 * @return a {@link Bean} used to share resources.
	 */
	@Bean
	public CorsFilter corsFilter() {

	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    CorsConfiguration config = new CorsConfiguration();
	    config.setAllowedOrigins(corsHosts);

	    config.addAllowedHeader("Date");
	    config.addAllowedHeader("Content-Type");
	    config.addAllowedHeader("Accept");
	    config.addAllowedHeader(" X-Requested-With");
	    config.addAllowedHeader("Authorization");
	    config.addAllowedHeader("From");
	    config.addAllowedHeader("X-Auth-Token");
	    config.addAllowedHeader("Request-Id");
	    config.addAllowedHeader("credentials");
	    
	    config.addExposedHeader("Set-Cookie");
	    
	    config.setAllowCredentials(true);
	    
	    config.addAllowedMethod("OPTIONS");
	    config.addAllowedMethod("HEAD");
	    config.addAllowedMethod("GET");
	    config.addAllowedMethod("PUT");
	    config.addAllowedMethod("POST");
	    config.addAllowedMethod("DELETE");
	    config.addAllowedMethod("PATCH");
	    
	    source.registerCorsConfiguration("/**", config);
	    return new CorsFilter(source);
	}
}
