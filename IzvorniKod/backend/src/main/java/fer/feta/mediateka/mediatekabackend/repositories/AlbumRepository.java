package fer.feta.mediateka.mediatekabackend.repositories;

import fer.feta.mediateka.mediatekabackend.models.Album;
import org.springframework.data.repository.CrudRepository;

public interface AlbumRepository extends CrudRepository<Album, Long> {
    Album findByTitle(String title);
}
