package fer.feta.mediateka.mediatekabackend.controllers;

import fer.feta.mediateka.mediatekabackend.controllers.validation.MediatekaUserDto;
import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintDefinitionException;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * Class is a controller that has an end point to which users registers to the application.
 * </pre>
 * 
 * @author hrvoje
 *
 */
@RestController
@RequestMapping("/register")
public class RegistrationController {
	
	/**
	 * Repository of all mediateka users
	 */
	@Autowired
	private MediatekaUserRepository userRepo;
	
	/**
	 * <pre>
	 * Method that is called when there is a POST request to /register end point.
	 * </pre>
	 * 
	 * @param user parameter that is a data transfer object for mediateka user. It stores
	 * 			all needed information to construct basic {@link MediatekaUser}
	 * @return returns a response entity dependent on response status, if there are no errors
	 * 			in body it returns constructed {@link MediatekaUser}
	 */
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<MediatekaUser> registerUser(@Valid @RequestBody MediatekaUserDto user) {
		MediatekaUser mediatekaUser = userRepo.save(user.getMediatekaUser());
		return ResponseEntity.ok(mediatekaUser);
	}
	
	/**
	 * <pre>
	 * Method intercepts any exception of type {@link MethodArgumentNotValidException} and
	 * extracts all error messages from the said exception and returns a map of those errors with
	 * a response status of 404.
	 * </pre>
	 * 
	 * @param ex {@link MethodArgumentNotValidException} instance
	 * @return map of errors where key is a name of a field that is not valid and value is a message that
	 * 			explains what is not right with the field
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = null;
	        fieldName = error instanceof FieldError ? ((FieldError) error).getField() : "confirmPassword";
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ConstraintDefinitionException.class)
	public Map<String, String> handleConstraintDefinitionException(
	  ConstraintDefinitionException ex) {
	    Map<String, String> errors = new HashMap<>();
	    errors.put("password", "Lozinke se ne podudaraju");
	    return errors;
	}
}
