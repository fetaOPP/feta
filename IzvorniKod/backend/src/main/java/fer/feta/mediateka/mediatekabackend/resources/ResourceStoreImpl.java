package fer.feta.mediateka.mediatekabackend.resources;

import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.HttpMethod;
import com.google.cloud.storage.Storage.SignUrlOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 * Class encapsulates methods that communicate with the resource server.
 * </pre>
 * 
 * @author hrvoje
 *
 */
@Service
public class ResourceStoreImpl implements ResourceStore {

	@Autowired
	private Bucket bucket;
	
	@Override
	public URL getFileGetLink(String fileName, long duration, TimeUnit timeUnit) {
		return bucket
				.get(fileName)
				.signUrl(duration, timeUnit, SignUrlOption.httpMethod(HttpMethod.GET), SignUrlOption.withV4Signature());
	}

	@Override
	public URL getFilePutLink(String fileName, long duration, TimeUnit timeUnit, String contentType) {
		BlobInfo blobInfo = BlobInfo
				.newBuilder(BlobId.of(bucket.getName(), fileName))
				.setContentType(contentType)
				.build();
		return bucket
				.getStorage()
				.signUrl(blobInfo, duration, timeUnit, SignUrlOption.withContentType(), SignUrlOption.httpMethod(HttpMethod.PUT), SignUrlOption.withV4Signature());
	}

	@Override
	public boolean containsItem(String name) {
		return bucket
				.get(name) != null;
	}

	@Override
	public URL getThumbnailPutLink(String name, long duration, TimeUnit timeUnit) {
		BlobInfo blobInfo = BlobInfo
				.newBuilder(BlobId.of(bucket.getName(), name))
				.setContentType("image/png")
				.build();
		return bucket
				.getStorage()
				.signUrl(blobInfo, duration, timeUnit, SignUrlOption.withContentType(), SignUrlOption.httpMethod(HttpMethod.PUT), SignUrlOption.withV4Signature());
	}


}
