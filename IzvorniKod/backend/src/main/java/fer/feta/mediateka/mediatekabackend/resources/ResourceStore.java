package fer.feta.mediateka.mediatekabackend.resources;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 * Interface that interacts with all file transfers. It encapsulates interface that is communicating with resource server.
 * </pre>
 * @author hrvoje
 *
 */
public interface ResourceStore {
	
	/**
	 * <pre>
	 * Method returns a signed url that allows user to make a get request to the resource. The request can be made in
	 * a span of <code>duration</code> of <code>timeUnit</code>. If request is made after that time singed url is not
	 * valid anymore and it will not get the requested file.
	 * </pre>
	 * 
	 * @param fileName name of the file that user request from the resource server
	 * @param duration time that the url is valid
	 * @param timeUnit time unit 
	 * @return url to the file, it's only availible method is get, any other request is not valid
	 */
	URL getFileGetLink(String fileName, long duration, TimeUnit timeUnit);
	
	/**
	 * <pre>
	 * Method returns a signed url that allows user to make a put request to resource server. The request can be made in
	 * a span of <code>duration</code> of <code>timeUnit</code>. If request is made after that time singed url is not
	 * valid anymore and it will not put the given data to resource server. Thumbnail must be in .png format, the supposed
	 * mime type is image/png.
	 * </pre>
	 * 
	 * @param name name of the thumbnail that is posted to the resource server
	 * @param duration time that the url is valid
	 * @param timeUnit time unit
	 * @return url to make a put request to, no other method can be requested through this url
	 */
	URL getThumbnailPutLink(String name, long duration, TimeUnit timeUnit);
	
	/**
	 * 
	 * <pre>
	 * Method returns a signed url that allows user to make a put request to resource server. The request can be made in
	 * a span of <code>duration</code> of <code>timeUnit</code>. If request is made after that time singed url is not
	 * valid anymore and it will not put the given data to resource server. Content type can specified by parameter.
	 * </pre>
	 * 
	 * @param fileName name of the file that is posted to the resource server
	 * @param duration time that the url is valid
	 * @param timeUnit time unit
	 * @param contentType type of the file
	 * @return
	 */
	URL getFilePutLink(String fileName, long duration, TimeUnit timeUnit, String contentType);

	/**
	 * <pre>
	 *     Method checks out if there already exists item with the name of <code>name</code>. If it
	 *     does returns true else it returns false.
	 * </pre>
	 * @param name name of the item that is searched for
	 * @return true if name already exists, false otherwise
	 */
	boolean containsItem(String name);
}
