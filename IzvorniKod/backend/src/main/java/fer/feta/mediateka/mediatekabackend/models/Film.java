package fer.feta.mediateka.mediatekabackend.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.Set;

/**
 * <pre>
 * This class encapsulates film persistence entity.
 * </pre>
 *
 * @author Mateo Imbrišak
 */

@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Film extends Item {

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Artist> directors;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Artist> producers;
}
