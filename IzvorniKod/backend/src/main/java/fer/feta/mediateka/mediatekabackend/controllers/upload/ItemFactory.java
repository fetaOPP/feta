package fer.feta.mediateka.mediatekabackend.controllers.upload;

import fer.feta.mediateka.mediatekabackend.controllers.upload.processors.*;
import fer.feta.mediateka.mediatekabackend.models.Item;
import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemFactory {

    @Autowired
    private FilmTypeItemRequestProcessor filmTypeItemRequestProcessor;

    @Autowired
    private BookTypeItemRequestProcessor bookTypeItemRequestProcessor;

    @Autowired
    private MagazineTypeItemRequestProcessor magazineTypeItemRequestProcessor;

    @Autowired
    private SongTypeItemRequestProcessor songTypeItemRequestProcessor;

    public Item getItem(ItemInfoDto dto, MediatekaUser currentUser) {
        RequestProcessor requestProcessor;
        switch (dto.getItemType().name()) {
            case "film":
                requestProcessor = filmTypeItemRequestProcessor;
                break;
            case "book":
                requestProcessor = bookTypeItemRequestProcessor;
                break;
            case "magazine":
                requestProcessor = magazineTypeItemRequestProcessor;
                break;
            case "song":
                requestProcessor = songTypeItemRequestProcessor;
                break;
            default:
                throw new IllegalArgumentException("There is no such type");
        }

        return requestProcessor.process(dto,currentUser);
    }
}
