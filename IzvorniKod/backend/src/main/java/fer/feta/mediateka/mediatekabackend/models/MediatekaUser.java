package fer.feta.mediateka.mediatekabackend.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Set;

/**
 * A class that models a user account persisted in the database.
 *
 * @author Mateo Imbrišak
 */

@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MediatekaUser {

    @Id @GeneratedValue
    private long id;

    @NotNull
    private String username;

    private String firstName;

    private String lastName;

    @NotNull
    private String payPalMail;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotNull
    private String passwordHash;

    @NotNull
    private RoleType role;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<Transaction> transactions;
}
