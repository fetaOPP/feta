package fer.feta.mediateka.mediatekabackend.controllers.validation;

import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * <pre>
 * This class is a validator class for checking if there exists username in the database that is trying to be 
 * inserted to database.
 * </pre>
 * 
 * @author hrvoje
 *
 */
@Component
public class UsernameValidator implements ConstraintValidator<ValidUsername, String> {

	/**
	 * Reference to user repository.
	 */
	@Autowired
	private MediatekaUserRepository userRepo;
	
	@Override
	public boolean isValid(String username, ConstraintValidatorContext context) {
		var user = userRepo.findByUsername(username);
		return  user == null;
	}

}
