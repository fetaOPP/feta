package fer.feta.mediateka.mediatekabackend.security;

import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.models.RoleType;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <pre>
 * Class encapsulates one implementation {@link UserDetailsService} that basically 
 * maps {@link MediatekaUser} to {@link UserDetails} object.
 * </pre>
 * 
 * @author hrvoje
 *
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

	/**
	 * Reference to user repository
	 */
	@Autowired
	private MediatekaUserRepository userRepo;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		var user = userRepo.findByUsername(username);
		return new User(user.getUsername(), user.getPasswordHash(), List.of(new AuthorityMapper(user.getRole())));
	}
	
	/**
	 * <pre>
	 * This is a adapter class for {@link RoleType} to {@link GrantedAuthority}.
	 * </pre>
	 * 
	 * @author hrvoje
	 *
	 */
	private static class AuthorityMapper implements GrantedAuthority {

		private static final long serialVersionUID = 1L;

		protected AuthorityMapper(RoleType type) {
			this.type = type;
		}

		private RoleType type;
		
		@Override
		public String getAuthority() {
			return type.name();
		}
		
	}

}
