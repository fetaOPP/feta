package fer.feta.mediateka.mediatekabackend.controllers;

import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.purchase.PurchaseType;
import fer.feta.mediateka.mediatekabackend.purchase.PurchasingService;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * <pre>
 *  This controller encapsulates an endpoint to which are send requests for purchasing items.
 * </pre>
 */
@RestController
@RequestMapping("/purchase")
public class PurchaseController {

    @Autowired
    private PurchasingService purchasingService;

    @Autowired
    private MediatekaUserRepository mediatekaUserRepository;

    /**
     * <pre>
     *      Method serves all GET requests to /purchase endpoint.
     * </pre>
     * @param purchaseType query parameter that tells what type of transaction this request is requesting
     * @param itemId query parameter that tells the id of the item that is trying to be bought by the user
     * @param principal currently logged in user that wants to do a transaction
     * @return {@link fer.feta.mediateka.mediatekabackend.purchase.PurchasingService.PurchaseInfo} that describes is
     *          transaction has succeeded and if not returns error messages describing what went wrong
     */
    @GetMapping
    public ResponseEntity<PurchasingService.PurchaseInfo> purchase(@RequestParam PurchaseType purchaseType, @RequestParam long itemId, Principal principal) {

        MediatekaUser currentUser = mediatekaUserRepository.findByUsername(principal.getName());

        PurchasingService.PurchaseInfo info = purchasingService.purchaseItem(purchaseType, itemId, currentUser);

        if(info.isSuccess()) return ResponseEntity.ok(info);

        return  ResponseEntity.badRequest().body(info);
    }

}
