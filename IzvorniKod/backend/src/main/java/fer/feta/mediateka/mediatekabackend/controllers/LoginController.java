package fer.feta.mediateka.mediatekabackend.controllers;

import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * Class encapsulates one rest controller that contains /login end point.
 * </pre>
 *
 * @author hrvoje
 *
 */
@RestController
public class LoginController {

	@Autowired
	private AuthenticationManager authManager;

	@Autowired
	private MediatekaUserRepository userRepo;

	@PostMapping(path = "/session", produces = "application/json", consumes = "application/json")
	public ResponseEntity<MediatekaUser> login(@RequestBody @Valid UserDto userDto, ModelMap model) {

		var authReq = new UsernamePasswordAuthenticationToken(userDto.getUsername(), userDto.getPassword());
		var auth = authManager.authenticate(authReq);

		SecurityContextHolder.getContext().setAuthentication(auth);
		return ResponseEntity.ok(userRepo.findByUsername(userDto.getUsername()));
	}


	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach((error) -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}

	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler(BadCredentialsException.class)
	public Map<String, String> handleBadCredentialException(BadCredentialsException ex) {
	    Map<String, String> errors = new HashMap<>();
	    errors.put("password", "Lozinka ili korisničko ime je krivo");
	    return errors;
	}

	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler(InternalAuthenticationServiceException.class)
	public Map<String, String> handleInternalAuthServiceException(InternalAuthenticationServiceException ex) {
	    Map<String, String> errors = new HashMap<>();
	    errors.put("password", "Lozinka ili korisničko ime je krivo");
	    return errors;
	}

	@Data
	private static class UserDto {

		@NotEmpty(message = "Korisničko ime ne smije biti prazno")
		private String username;

		@NotEmpty(message = "Lozinka ne smije biti prazna")
		private String password;
	}
}
