package fer.feta.mediateka.mediatekabackend.models;

/**
 * <pre>
 * This class is made to hold all constants that classes in package {@code fer.feta.mediatekabackend.models} need.
 * </pre>
 * @author hrvoje
 *
 */
public class ModelsConstants {

	public static final int ITEM_DESCRIPTION_MAX_LENGTH = 50;
	
	public static final int ITEM_PURCHASE_PRICE_PRECISION = 10;
	
	public static final int ITEM_PURCHASE_PRICE_SCALE = 5;

	public static final int PUBLISHER_NAME_MAX_LENGTH = 20;
}
