package fer.feta.mediateka.mediatekabackend.repositories;

import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import org.springframework.data.repository.CrudRepository;

/**
 * <pre>
 * Interface is made for spring framework to implement it.
 * </pre>
 * 
 * @author hrvoje
 *
 */
public interface MediatekaUserRepository extends CrudRepository<MediatekaUser, Long> {
	
	/**
	 * <pre>
	 * Returns {@link MediatekaUser} by username that is given.
	 * </pre>
	 * 
	 * @param username given username
	 * @return {@link MediatekaUser} with given username
	 */
	MediatekaUser findByUsername(String username);
	
	/**
	 * <pre>
	 * Returns {@link MediatekaUser} by email that is given.
	 * </pre>
	 * 
	 * @param email given email
	 * @return {@link MediatekaUser} with given email
	 */
	MediatekaUser findBypayPalMail(String email);
}
