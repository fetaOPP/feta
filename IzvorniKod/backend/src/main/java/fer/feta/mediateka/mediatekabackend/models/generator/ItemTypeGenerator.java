package fer.feta.mediateka.mediatekabackend.models.generator;

import fer.feta.mediateka.mediatekabackend.models.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.tuple.ValueGenerator;

/**
 * <pre>
 * This class is a generator for the type attribute in class {@link Item} that fills
 * in the type dependent on instance class.
 * </pre>
 * @author hrvoje
 *
 */
public class ItemTypeGenerator implements ValueGenerator<ItemType> {

	@Override
	public ItemType generateValue(Session session, Object owner) {
		if(owner instanceof Book) return ItemType.book;
		else if(owner instanceof Magazine) return ItemType.magazine;
		else if(owner instanceof Film) return ItemType.film;
		else if(owner instanceof Song) return ItemType.song;

		throw new HibernateException("There is no such item type!");
	}

}
