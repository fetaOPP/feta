package fer.feta.mediateka.mediatekabackend.repositories;

import fer.feta.mediateka.mediatekabackend.models.Item;
import org.springframework.data.repository.CrudRepository;

/**
 * <pre>
 * This interface represents a repository for all entities of type {@code Item}. If you look for the implementation
 * of this interface in the project you will not find it. That is because Spring Boot searches by default for
 * every interface that extends the {@code CrudRepository} and generates its implementation in runtime. To get
 * a reference to that generated implementation of interface {@code ItemRepository} you should look at usage of
 * {@code @Autowired} annotation.
 * 
 * Example how use {@code @Autowired} annotation:
 * 		
 * 		@Autowired
 * 		ItemRepository itemRepository;
 * 
 * In this example Spring Framework will inject the generated {@code ItemRepository} implementation into 
 * <code>itemRepository</code> reference.
 * </pre>
 * 
 * @author hrvoje
 *
 */
public interface ItemRepository extends CrudRepository<Item, Long> {}
