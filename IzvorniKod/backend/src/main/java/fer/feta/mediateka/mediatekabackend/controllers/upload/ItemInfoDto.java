package fer.feta.mediateka.mediatekabackend.controllers.upload;

import fer.feta.mediateka.mediatekabackend.models.ItemType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Set;

/**
 * <pre>
 *      This class encapsulates all possible attributes that can be given in item uploading requests.
 *      Attributes are sorted in six categories:
 *          <ul>
 *              <li>Item - generic category all request items have these attributes</li>
 *              <li>Film - attributes that film item has in respect to Item</li>
 *              <li>Book - attributes that book item has in respect to two above</li>
 *              <li>Magazine - attributes that magazine type has in respect of three above</li>
 *              <li>Song - attributes that song type has in respect of four above</li>
 *          </ul>
 *
 *      Attribute pairs in form of (${attribute}, ${attribute}Id/Ids) are here only for linking existing entities
 *      to the one in making or to create a new entity with making of a new one.
 *
 *      Perhaps if you look at pair (publishers, publishersIds) you would use publisher field to create new publishers
 *      by typing their names like this "publisher1,publisher2". But if you want to link the existing publisher to
 *      some other item you would use publisherIds field where you would list all ids of publishers that you want
 *      to link with the item in creation.
 *
 *      You can use both of these simultaneously.
 *
 * </pre>
 */
@Data
public class ItemInfoDto {

    // If item already exists
    private Long id;

    // Item
    private String title;

    private String publishers;
    private Set<Long> publishersIds;

    private ItemType itemType;
    private int yearOfIssue;
    private String description;
    private BigDecimal purchasePrice;
    private boolean available;


    // Film
    private String directors;
    private Set<Long> directorsIds;

    private String producers;
    private Set<Long> producersIds;


    // Book
    private String authors;
    private Set<Long> authorsIds;


    // Magazine
    private int issueNumber;
    private int monthOfIssue;


    // Song
    private short trackNumber;

    private Set<Long> artistsIds;
    private String artists;

    private String songwriters;
    private Set<Long> songwritersIds;

    private String album;
}
