package fer.feta.mediateka.mediatekabackend.controllers.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <pre>
 * Annotation that is used to annotate a field that keeps the first name and last name as a {@link String}.
 * It uses {@link NameValidator} to check if the given name contains any illegal characters.
 * </pre>
 *
 * @author Mateo Imbrišak
 */

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = NameValidator.class)
@Documented
public @interface ValidName {
    String message() default "Ime smije sadržati samo slova";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
