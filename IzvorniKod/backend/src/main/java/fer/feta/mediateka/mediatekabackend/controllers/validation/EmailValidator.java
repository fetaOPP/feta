package fer.feta.mediateka.mediatekabackend.controllers.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <pre>
 * Class validates that the email is in right format.
 * </pre>
 * 
 * @author hrvoje
 *
 */
public class EmailValidator implements ConstraintValidator<ValidEmail, String> {

	private Pattern pattern;
    private Matcher matcher;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$"; 
	
	@Override
	public boolean isValid(String email, ConstraintValidatorContext context) {
		return validateEmail(email);
	}
	
	/**
	 * <pre>
	 * Uses {@link EmailValidator#EMAIL_PATTERN} to see if given email is valid. Dependent on the given regex returns {@code true} or {@code false}.
	 * </pre>
	 * 
	 * @param email provided email
	 * @return {@code true} if email is in right format, else {@code false}
	 */
	private boolean validateEmail(String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
