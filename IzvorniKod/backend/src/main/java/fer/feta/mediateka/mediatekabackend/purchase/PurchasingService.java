package fer.feta.mediateka.mediatekabackend.purchase;

import fer.feta.mediateka.mediatekabackend.models.Item;
import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.models.Transaction;
import fer.feta.mediateka.mediatekabackend.models.TransactionType;
import fer.feta.mediateka.mediatekabackend.paypal.PayPalService;
import fer.feta.mediateka.mediatekabackend.repositories.ItemRepository;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import fer.feta.mediateka.mediatekabackend.repositories.TransactionRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

/**
 * <pre>
 *     Class encapsulates all relevant methods and classes that are required for buying the item from the application.
 * </pre>
 */

@Service
public class PurchasingService {

    @Autowired
    private PayPalService payPalService;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    /**
     * <pre>
     *     Method dependent on user attribute attempts to make a purchase or rental dependent on purchaseType.
     *     If transaction is not successful then the returning object of {@link PurchaseInfo} has an error message
     *     describing what went wrong. Otherwise {@link PurchaseInfo} has not messages and its method
     *     {@link PurchaseInfo#isSuccess()} returns true.
     * </pre>
     *
     * @param purchaseType type of purchase see all types in {@link PurchaseType}
     * @param itemId item id that wants to be bought by user
     * @param user user that wants to make a transaction
     * @return instance of {@link PurchaseInfo} that encapsulates error messages if any
     */
    public PurchaseInfo purchaseItem(PurchaseType purchaseType, long itemId, MediatekaUser user) {
        PurchaseInfo info = new PurchaseInfo();

        Optional<Item> item = itemRepository.findById(itemId);
        if(item.isEmpty()) info.addError("item", "Ovaj sadržaj ne postoji");
        if(item.isPresent() && !item.get().isAvailable()) info.addError("item", "Ovaj sadržaj nije dostupan");

        boolean isItemAlreadyBoughtAndValidByTheUser = user
                .getTransactions()
                .stream()
                .filter(t -> item.get().getId() == t.getItem().getId())
                .anyMatch(Transaction::isValid);

        if(isItemAlreadyBoughtAndValidByTheUser) info.addError("item", "Ovaj sadržaj je već kupljen");

        if(item.isPresent() && !isItemAlreadyBoughtAndValidByTheUser) {
            BigDecimal transactionPrice = calculateCost(purchaseType, item.get());

            if(!payPalService.makeATransaction(transactionPrice, user)) info.addError("item", "Transakcija se ne može izvršiti");

            if(!info.isSuccess()) return  info;

            Date currentDate = new Date();

            Transaction transaction = Transaction.builder()
                    .item(item.get())
                    .timeOfTransaction(currentDate)
                    .type(purchaseType == PurchaseType.BUY ? TransactionType.PURCHASE : TransactionType.RENTAL)
                    .user(user)
                    .validUntil(calculateValidUntil(purchaseType, currentDate))
                    .build();

            transactionRepository.save(transaction);
        }

        return info;
    }

    /**
     * <pre>
     *     Method calculates the date to which current transaction is valid. In case where the user has bought
     *     the item it returns the currentDate reference.
     * </pre>
     *
     * @param purchaseType type of purchase see all types in {@link PurchaseType}
     * @param currentDate date when the transaction was made
     * @return date to which transaction is valid
     */
    private Date calculateValidUntil(PurchaseType purchaseType, Date currentDate) {
        long time = currentDate.getTime();
        switch (purchaseType) {
            case BUY:
                return currentDate;
            case DAY:
                return new Date(time+1*24*60*60*1000L);
            case TWO_DAY:
                return new Date(time+2*24*60*60*1000L);
            case WEEK:
                return new Date(time+7*24*60*60*1000L);
            case MONTH:
                return new Date(time+30*24*60*60*1000L);
        }
        return currentDate;
    }

    /**
     * <pre>
     *      Calculates the cost of the transaction depended on item that is purchased and purchaseType.
     * </pre>
     *
     * @param purchaseType type of purchase see all types in {@link PurchaseType}
     * @param item item that is going to be bought
     * @return cost of transaction
     */
    private BigDecimal calculateCost(PurchaseType purchaseType, Item item) {
        BigDecimal buyPrice = item.getPurchasePrice();

        if(purchaseType == PurchaseType.DAY) return buyPrice.multiply(BigDecimal.valueOf(0.2)).round(new MathContext(2));
        else if(purchaseType == PurchaseType.TWO_DAY) return buyPrice.multiply(BigDecimal.valueOf(0.4)).round(new MathContext(2));
        else if(purchaseType == PurchaseType.WEEK) return buyPrice.multiply(BigDecimal.valueOf(0.6)).round(new MathContext(2));
        else if(purchaseType == PurchaseType.MONTH) return buyPrice.multiply(BigDecimal.valueOf(0.8)).round(new MathContext(2));
        return buyPrice.round(new MathContext(2));
    }

    /**
     * <pre>
     *     Class is used for encapsulating error messages and telling the caller of
     *     {@link PurchasingService#purchaseItem(PurchaseType, long, MediatekaUser)} if transaction has succeeded.
     * </pre>
     */
    @Data
    public static class PurchaseInfo {
        private Map<String, List<String>> errors = new HashMap<>();

        private boolean success;

        /**
         * <pre>
         *     Method adds an error message to be associated to the key. Key is the subject that is somehow
         *     connected with the error, and the message describes what went wrong with that subject.
         * </pre>
         * @param key subject of error message
         * @param message error message that describes what went wrong
         */
        public void addError(String key, String message) {
            errors.merge(key, new ArrayList<>(List.of(message)), (k, list) -> {
                list.add(message);
                return list;
            });
        }

        /**
         * <pre>
         *     Method returns true if there is no error messages otherwise it returns false
         * </pre>
         * @return true if no error messages, false otherwise
         */
        public boolean isSuccess() {
            return errors.isEmpty();
        }
    }
}
