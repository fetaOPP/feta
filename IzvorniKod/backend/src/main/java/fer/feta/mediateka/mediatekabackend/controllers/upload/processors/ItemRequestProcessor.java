package fer.feta.mediateka.mediatekabackend.controllers.upload.processors;

import fer.feta.mediateka.mediatekabackend.controllers.upload.ItemInfoDto;
import fer.feta.mediateka.mediatekabackend.models.Item;
import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.models.Publisher;
import fer.feta.mediateka.mediatekabackend.models.RoleType;
import fer.feta.mediateka.mediatekabackend.repositories.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * <pre>
 *     This processor is not a typical {@link RequestProcessor}, this one will
 *     encapsulate all code that would be repetitive in all other processors.
 * </pre>
 */
@Component
public class ItemRequestProcessor {

    @Autowired
    private PublisherRepository publisherRepository;

    public <T extends Item> T processItem(ItemInfoDto dto, MediatekaUser user, T itemReference) {
        itemReference.setTitle(dto.getTitle());
        itemReference.setDescription(dto.getDescription());
        itemReference.setAvailable(user.getRole() == RoleType.ADMIN && dto.isAvailable());

        Set<Publisher> publishersNew =
                dto.getPublishers()==null ?
                        null :
                        Set.of(dto.getPublishers().split(","))
                                .stream()
                                .map(str->Publisher.builder().name(str).build())
                                .collect(Collectors.toSet());
        Set<Publisher> publishersLinked =
                dto.getArtistsIds()==null ?
                        null :
                        StreamSupport
                                .stream(publisherRepository.findAllById(
                                        dto.getArtistsIds()).spliterator(), false)
                                .collect(Collectors.toSet());

        Set<Publisher> publishers = new HashSet<>();
        if(publishersNew != null) publishers.addAll(publishersNew);
        if(publishersLinked != null) publishers.addAll(publishersLinked);

        itemReference.setPublishers(publishers);
        itemReference.setType(dto.getItemType());
        itemReference.setPurchasePrice(dto.getPurchasePrice());
        itemReference.setYearOfIssue(dto.getYearOfIssue());

        return itemReference;
    }
}
