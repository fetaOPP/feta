package fer.feta.mediateka.mediatekabackend.controllers;

import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.repositories.MediatekaUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

/**
 * <pre>
 * Class encapsulates a controller which only end point returns current session {@link MediatekaUser}.
 * </pre>
 * 
 * @author hrvoje
 *
 */
@Controller
public class CurrentUserController {

	/**
	 * Reference to user repository
	 */
	@Autowired
	private MediatekaUserRepository userRepo;
	
	/**
	 * <pre>
	 * Method is called every time that there is a GET request to /session end point. It returns
	 * current session {@link MediatekaUser}.
	 * </pre>
	 * 
	 * @param principal object that contains username of current {@link MediatekaUser} session user
	 * @return response entity dependent if there even exists session user(in that case it returns code 404)
	 * 			or if there is such user then it returns code 200 with the body of current session user.
	 */
	@GetMapping(path = "/session")
	public ResponseEntity<MediatekaUser> getCurrentSessionUser(Principal principal) {
		if(principal == null) return ResponseEntity.notFound().build();
		
		MediatekaUser user = userRepo.findByUsername(principal.getName());
		return ResponseEntity.ok(user);
	}
}
