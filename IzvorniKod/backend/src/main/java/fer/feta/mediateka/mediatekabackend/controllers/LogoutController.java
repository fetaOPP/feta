package fer.feta.mediateka.mediatekabackend.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <pre>
 *     Class encapsulates logout endpoint that is used to logout from application.
 * </pre>
 */
@RestController
public class LogoutController {

    /**
     * <pre>
     *     Method takes no parameters and logout current {@link java.security.Principal} e.g. session user.
     *     Method always returns {@link ResponseEntity} with status OK(200), even if there is no session user
     *     to be logged out.
     * </pre>
     *
     * @return {@link ResponseEntity<Void>}, every time with the status 200, e.g. OK
     */
    @DeleteMapping(path = "/session")
    public ResponseEntity<Void> logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return ResponseEntity.ok().build();
    }

}
