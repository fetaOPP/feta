package fer.feta.mediateka.mediatekabackend.paypal;

import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <pre>
 *     Class encapsulates communication to PayPal online service which is trying to make a transaction(e.g. purchase)
 *     on the account of the user.
 * </pre>
 */
@Service
public class PayPalService {

    /**
     * <pre>
     *     Method tries to make a transaction dependent on the cost of transaction and the user that wants to make
     *     a transaction and returns if it succeeded.
     * </pre>
     * @param cost cost of transaction(e.g. item that wants to be bought)
     * @param user user that wants to make a transaction
     * @return true is transaction is successful, false otherwise
     */
    public boolean makeATransaction(BigDecimal cost, MediatekaUser user) {

        // TODO
        // Algorithm for making a transaction
        // Currently it always returns true, that indicates that transaction is always successful

        return true;
    }

}
