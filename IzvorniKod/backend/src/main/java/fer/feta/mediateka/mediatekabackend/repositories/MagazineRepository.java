package fer.feta.mediateka.mediatekabackend.repositories;

import fer.feta.mediateka.mediatekabackend.models.Magazine;
import org.springframework.data.repository.CrudRepository;

public interface MagazineRepository extends CrudRepository<Magazine, Long> {}
