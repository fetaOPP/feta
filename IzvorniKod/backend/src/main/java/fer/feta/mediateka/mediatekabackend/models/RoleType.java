package fer.feta.mediateka.mediatekabackend.models;

/**
 * Defines access level for a {@link MediatekaUser}.
 *
 * @author Mateo Imbrišak
 */

public enum RoleType {

    /**
     * Standard user that can buy items.
     */
    USER,

    /**
     * User that can also add and remove items from the shop.
     */
    COORDINATOR,

    /**
     * User that has all privileges of {@link #COORDINATOR}
     * and can also edit other user's info.
     */
    ADMIN
}
