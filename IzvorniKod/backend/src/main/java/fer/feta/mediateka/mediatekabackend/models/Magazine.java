package fer.feta.mediateka.mediatekabackend.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;

/**
 * <pre>
 * This class encapsulates magazine persistence entity.
 * </pre>
 *
 * @author Mateo Imbrišak
 */

@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Magazine extends Item {

    private int issueNumber;

    private int monthOfIssue;
}
