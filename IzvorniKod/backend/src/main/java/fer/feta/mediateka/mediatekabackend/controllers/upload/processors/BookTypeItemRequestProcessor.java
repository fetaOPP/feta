package fer.feta.mediateka.mediatekabackend.controllers.upload.processors;

import fer.feta.mediateka.mediatekabackend.controllers.upload.ItemInfoDto;
import fer.feta.mediateka.mediatekabackend.models.Artist;
import fer.feta.mediateka.mediatekabackend.models.Book;
import fer.feta.mediateka.mediatekabackend.models.Item;
import fer.feta.mediateka.mediatekabackend.models.MediatekaUser;
import fer.feta.mediateka.mediatekabackend.repositories.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class BookTypeItemRequestProcessor implements RequestProcessor {

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private ItemRequestProcessor itemPreprocessor;

    @Override
    public Item process(ItemInfoDto dto, MediatekaUser user) {
        Set<Artist> authorsNew = dto.getDirectors()==null ?
                null :
                Set.of(dto.getAuthors().split(","))
                        .stream().map(str->Artist.builder().name(str).build()).collect(Collectors.toSet());
        Set<Artist> authorsLinked = dto.getAuthorsIds()==null ?
                null :
                StreamSupport
                        .stream(artistRepository.findAllById(dto.getAuthorsIds()).spliterator(), false)
                        .collect(Collectors.toSet());


        Set<Artist> authors = new HashSet<>();
        if(authorsNew != null) authors.addAll(authorsNew);
        if(authorsLinked != null) authors.addAll(authorsLinked);

        Book newBook = Book
                .builder()
                    .authors(authors)
                .build();

        return itemPreprocessor.processItem(dto,user,newBook);
    }
}
