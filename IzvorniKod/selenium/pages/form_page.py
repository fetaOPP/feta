from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.nav_bar_page import NavBarPage


class FormPage(NavBarPage):
    """A class representing all Mediateka pages that contain forms."""

    _FORM_INPUT_XPATH = "//form//input"
    _FORM_INPUT_WITH_PLACEHOLDER_XPATH = _FORM_INPUT_XPATH + "[@placeholder='{}']"
    _SUBMIT_BUTTON_XPATH = "//form/button[@type='submit']"
    _INVALID_INPUT_MESSAGE_XPATH = "//div[@class='ant-typography ant-typography-danger']"

    def __init__(self, driver):
        """
        Initializes a FormPage instance.

        :param driver: the webdriver instance to use
        """
        super().__init__(driver)

    def fill_in_form(self, form_data):
        """
        Fills in the form with the given data.

        :param form_data: the data to enter into the form fields
        """
        form = WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located(
            (By.XPATH, self._FORM_INPUT_XPATH)))

        for input_data, form_input in zip(form_data, form):
            form_input.clear()
            form_input.send_keys(input_data)

    def fill_in_form_input(self, input_placeholder, input_data):
        """
        Fills in the specified form input with the given data.

        :param input_placeholder: the input's placeholder to use for selecting it
        :param input_data: the data to enter into the form input
        """
        form_input = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH, self._FORM_INPUT_WITH_PLACEHOLDER_XPATH.format(input_placeholder))))

        form_input.clear()
        form_input.send_keys(input_data)

    def submit_form(self):
        """Submits the form and waits for the URL to change."""
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, self._SUBMIT_BUTTON_XPATH))).click()

        WebDriverWait(self.driver, 10).until(EC.url_changes)

    def get_invalid_form_messages(self):
        """Returns all messages shown after submitting an invalid form."""
        message_elements = WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, self._INVALID_INPUT_MESSAGE_XPATH)))

        mapped_messages = map(lambda element: element.text, message_elements)

        return list(mapped_messages)
