from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class NavBarPage:
    """The base class for all Mediateka app pages."""

    _HOME_ICON_XPATH = "//div[@class='navigation']/a/img"
    _NAVBAR_BUTTON_XPATH = "//button/span[contains(text(), '{}')]/.."
    _DROPDOWN_BUTTON_XPATH = "//button[@class='ant-btn ant-dropdown-trigger']"
    _DROPDOWN_ITEM_XPATH = "//ul[contains(@class, 'ant-dropdown-menu')]//*[contains(text(), '{}')]"
    _NOTIFICATION_X_XPATH = "//span[@class='ant-notification-close-x']"
    _NOTIFICATION_MESSAGE_CLASS = "ant-notification-notice-message"
    _NOTIFICATION_DESCRIPTION_CLASS = "ant-notification-notice-description"

    def __init__(self, driver):
        """
        Initializes a NavBarPage instance.

        :param driver: the webdriver instance to use
        """
        self.driver = driver

    def go_home(self):
        """Navigates to the homepage."""
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, self._HOME_ICON_XPATH))).click()

    def click_on_navbar_button(self, button_text):
        """
        Clicks on a navbar button specified by the given text and waits for the URL to change.

        :param button_text: the text to use for selecting the navbar button ("Registracija" / "Prijava")
        """
        if button_text not in ("Registracija", "Prijava"):
            raise ValueError("Unknown navbar button text " + button_text)

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, self._NAVBAR_BUTTON_XPATH.format(button_text)))).click()

        expected_url_path = '/register' if button_text == 'Registracija' else '/login'
        WebDriverWait(self.driver, 10).until(EC.url_contains(expected_url_path))

    def expand_dropdown(self):
        """
        Expands the navbar dropdown menu.

        NOTE: The dropdown is only visible when a user is logged in.
        """
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, self._DROPDOWN_BUTTON_XPATH))).click()

    def click_on_dropdown_item(self, item_text):
        """
        Clicks on the dropdown item specified by the given text.

        :param item_text: the text to use for selecting the item
        """
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, self._DROPDOWN_ITEM_XPATH.format(item_text)))).click()

    def get_notification_text(self):
        """Returns the text contained in a notification pop-up."""
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, self._NOTIFICATION_X_XPATH)))

        message = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(
            (By.CLASS_NAME, self._NOTIFICATION_MESSAGE_CLASS))).text
        description = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(
            (By.CLASS_NAME, self._NOTIFICATION_DESCRIPTION_CLASS))).text

        return [message, description]

    def close_notification(self):
        """Closes the notification pop-up."""
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, self._NOTIFICATION_X_XPATH))).click()

    def get_url_path(self):
        """Returns the path of the current URL."""
        from urllib.parse import urlparse

        return urlparse(self.driver.current_url).path
