import unittest
from pathlib import Path

import yaml
from selenium import webdriver

from api.mediateka_api import MediatekaAPI
from pages.form_page import FormPage
from pages.nav_bar_page import NavBarPage as HomePage


class TestLogin(unittest.TestCase):
    _MEDIATEKA_URL = 'http://mediateka.herokuapp.com/'
    _TEST_DATA_PATH = (Path(__file__).parents[1] / 'data/test_data.yaml').resolve()

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.get(cls._MEDIATEKA_URL)

        with open(cls._TEST_DATA_PATH, 'r') as stream:
            test_data_file = yaml.safe_load(stream)
            testuser_json = test_data_file['testuser_json']
            cls.login_test_data = test_data_file['test_login']

        MediatekaAPI().create_user(testuser_json)

    def test_21_successful_login_and_logout(self):
        test_data = self.login_test_data['test_21']

        home_page = HomePage(self.driver)
        home_page.click_on_navbar_button('Prijava')

        login_page = FormPage(self.driver)
        login_page.fill_in_form(test_data['form_data'])
        login_page.submit_form()

        home_page = HomePage(self.driver)
        self.assertIn(test_data['login_success_message'], home_page.get_notification_text())

        home_page.close_notification()

        home_page.expand_dropdown()
        home_page.click_on_dropdown_item('Odjava')
        self.assertIn(test_data['logout_success_message'], home_page.get_notification_text())

    def test_22_invalid_credentials(self):
        test_data = self.login_test_data['test_22']

        home_page = HomePage(self.driver)
        home_page.click_on_navbar_button('Prijava')

        login_page = FormPage(self.driver)

        for test_iteration in test_data.values():
            login_page.fill_in_form(test_iteration['form_data'])
            login_page.submit_form()

            self.assertEqual(login_page.get_invalid_form_messages(), test_iteration['invalid_form_messages'])
            self.driver.refresh()

    def test_23_submitting_incomplete_form(self):
        test_data = self.login_test_data['test_23']

        home_page = HomePage(self.driver)
        home_page.click_on_navbar_button('Prijava')

        login_page = FormPage(self.driver)
        login_page.submit_form()

        self.assertEqual(login_page.get_invalid_form_messages(), test_data['invalid_form_messages'])
        self.driver.refresh()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


if __name__ == '__main__':
    unittest.main()
