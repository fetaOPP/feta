import re
import unittest
from datetime import datetime
from pathlib import Path

import yaml
from selenium import webdriver

from api.mediateka_api import MediatekaAPI
from pages.form_page import FormPage
from pages.nav_bar_page import NavBarPage as HomePage


class TestRegistration(unittest.TestCase):
    _MEDIATEKA_URL = 'http://mediateka.herokuapp.com/'
    _TEST_DATA_PATH = (Path(__file__).parents[1] / 'data/test_data.yaml').resolve()

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.get(cls._MEDIATEKA_URL)

        with open(cls._TEST_DATA_PATH, 'r') as stream:
            test_data_file = yaml.safe_load(stream)
            testuser_json = test_data_file['testuser_json']
            cls.registration_test_data = test_data_file['test_registration']

        MediatekaAPI().create_user(testuser_json)

    def test_11_successful_registration(self):
        test_data = self.registration_test_data['test_11']
        random_string = re.sub('[\\s+:.-]', '', str(datetime.now()))

        home_page = HomePage(self.driver)
        home_page.click_on_navbar_button('Registracija')

        register_page = FormPage(self.driver)
        register_page.fill_in_form(test_data['form_data'])
        register_page.fill_in_form_input('Korisničko ime', random_string)
        register_page.fill_in_form_input('E-mail', random_string + '@mail.com')
        register_page.submit_form()

        login_page = FormPage(self.driver)
        self.assertEqual(login_page.get_notification_text(), test_data['registration_success_message'])

    def test_12_submitting_incomplete_form(self):
        test_data = self.registration_test_data['test_12']

        home_page = HomePage(self.driver)
        home_page.click_on_navbar_button('Registracija')

        register_page = FormPage(self.driver)
        register_page.submit_form()

        self.assertEqual(register_page.get_invalid_form_messages(), test_data['invalid_form_messages'])
        self.driver.refresh()

    def test_13_invalid_email_format(self):
        test_data = self.registration_test_data['test_13']

        home_page = HomePage(self.driver)
        home_page.click_on_navbar_button('Registracija')

        register_page = FormPage(self.driver)
        register_page.fill_in_form(test_data['form_data'])

        for test_iteration in test_data['iterations'].values():
            register_page.fill_in_form_input('E-mail', test_iteration['invalid_email'])
            register_page.submit_form()

            self.assertEqual(register_page.get_invalid_form_messages(), test_data['invalid_form_messages'])

    def test_14_password_confirmation(self):
        test_data = self.registration_test_data['test_14']

        home_page = HomePage(self.driver)
        home_page.click_on_navbar_button('Registracija')

        register_page = FormPage(self.driver)
        register_page.fill_in_form(test_data['form_data'])
        register_page.submit_form()

        self.assertEqual(register_page.get_invalid_form_messages(), test_data['invalid_form_messages'])
        self.driver.refresh()

    def test_15_existing_username_and_email(self):
        test_data = self.registration_test_data['test_15']

        home_page = HomePage(self.driver)
        home_page.click_on_navbar_button('Registracija')

        register_page = FormPage(self.driver)

        for test_iteration in test_data.values():
            register_page.fill_in_form(test_iteration['form_data'])
            register_page.submit_form()

            self.assertEqual(register_page.get_invalid_form_messages(), test_iteration['invalid_form_messages'])
            self.driver.refresh()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


if __name__ == '__main__':
    unittest.main()
