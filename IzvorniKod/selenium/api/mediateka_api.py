import requests


class MediatekaAPI:
    """A class for sending API requests to the Mediateka app."""
    _API_BASE_URL = 'http://mediateka-api.herokuapp.com'

    _REGISTER_ENDPOINT = '/register'
    _SESSION_ENDPOINT = '/session'
    _USER_ENDPOINT = '/api/mediatekaUser'

    def create_user(self, registration_json):
        """
        Registers the specified user.

        :param registration_json: a dict representing the registration JSON data to send
        :return: the HTTP response
        """
        return requests.post(self._API_BASE_URL + self._REGISTER_ENDPOINT, json=registration_json)

    def delete_user(self, user_id, cookies):
        """
        Deletes the user of the specified id.

        :param cookies: the cookies to user when making the HTTP request
        :param user_id: the id of the user to delete
        :return: the HTTP response
        """
        return requests.delete(self._API_BASE_URL + self._USER_ENDPOINT + f'/{user_id}', cookies=cookies)

    def create_session(self, login_json):
        """
        Logs in the specified user.

        :param login_json: a dict representing the login JSON data to send
        :return: the HTTP response
        """
        return requests.post(self._API_BASE_URL + self._SESSION_ENDPOINT, json=login_json).cookies

    def find_user_ids_for(self, username, email, cookies):
        """
        Returns the ids of all users that contain the specified username and/or email.

        :param cookies: the cookies to user when making the HTTP request
        :param username: the username of the users to find
        :param email: the email of the users to find
        :return: the ids of all users that contain the specified username and/or email
        """
        user_filter = f'?filter[username]={username}&filter[payPalMail]={email}'

        user_data = requests.get(self._API_BASE_URL + self._USER_ENDPOINT + user_filter, cookies=cookies).json()['data']

        if not user_data:
            return None
        return [user['id'] for user in user_data]
