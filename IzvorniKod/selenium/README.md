# Mediateka Selenium tests

Selenium tests for verifying login, logout, and registration functionalities.

## Getting Started

Download or clone the [Feta](https://gitlab.com/fetaOPP/feta) repository.

### Prerequisites

* Python 3.x - [Download & install the latest version](https://www.python.org/downloads/). Starting with Python 3.4, `pip` is included by default with the Python binary installers.
* Selenium - Easiest to install with `pip`. [Instructions](https://selenium-python.readthedocs.io/installation.html).
    ```shell script
    $ pip install selenium
    ```
* PyYAML - Easiest to install with `pip`. [Instructions](https://pyyaml.org/wiki/PyYAMLDocumentation).
    ```shell script
    $ pip install pyyaml
    ```
* Requests - Easiest to install with `pip`. [Other installation options](https://requests.readthedocs.io/en/master/user/install/).
    ```shell script
    $ pip install requests
    ```
* ChromeDriver - [Download](http://www.mongodb.org/downloads), and [include the ChromeDriver location](https://chromedriver.chromium.org/getting-started) in your `PATH` environment variable (macOS: just move the binary to `/usr/local/bin`).

## Running the tests

On the command-line, `cd` to `Feta/IzvorniKod/selenium`. 

All tests in a given test file can then be run with the following command: 
```shell script
$ python -m unittest path/to/test_file.py
```

If you also have Python 2.x installed, make sure to use `python3` instead of just `python`.

For example, execute the following command to run the login page tests:
```shell script
$ python3 -m unittest tests/test_login.py
```

Alternatively, open the `Feta/IzvorniKod/selenium` directory in an IDE (such as [PyCharm](https://www.jetbrains.com/pycharm/)) and run the tests from there.
