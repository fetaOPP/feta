export const ItemCategory = {
  Film: 'film',
  Book: 'book',
  Magazine: 'magazine',
  Song: 'song',
};
