import { useHistory } from 'react-router-dom';

export function useQuery() {
  const history = useHistory();

  const params = new URLSearchParams(history.location.search);

  const updateHistory = () => {
    history.push(`${history.location.pathname}?${params.toString()}`);
  }

  const updateParams = (key, value, shouldUpdateHistory = true) => {
    if (value) {
      params.set(key, value);
    } else {
      params.delete(key);
    }

    if (shouldUpdateHistory) {
      updateHistory();
    }
  }

  return [
    params,
    updateParams,
    updateHistory,
  ];
}
