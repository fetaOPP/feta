import { useState, useEffect } from 'react';
import { fetchItems, createCategoryItem } from '../services/product';
import { useQuery } from './useQuery';

export function useSearch(initialParams = {}) {
  const [totalCount, setTotalCount] = useState(0);
  const [params, updateParams] = useQuery();
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const onFetch = async () => {
    setLoading(true);

    const items = await fetchItems(params);
    setItems(items.data.map((item) => createCategoryItem(item)));
    setTotalCount(items.meta.totalResourceCount);

    setLoading(false);
  }

  useEffect(() => {
    Object.keys(initialParams).forEach((key) => updateParams(key, initialParams[key]));
  }, []);

  useEffect(() => {
    onFetch();
    return () => {};
  }, [params.toString()]);

  return {
    items,
    loading,
    params,
    updateParams,
    totalCount,
  };
}
