import { useQuery } from './useQuery';

export function useArrayParams(queryKey) {
  const [params] = useQuery();

  const arrayParams = params.has(queryKey) ? params.get(queryKey).split(',') : [];

  const toggleArrayParam = (key) => {
    if (arrayParams.includes(key)) {
      arrayParams.splice(arrayParams.indexOf(key), 1);
    } else {
      arrayParams.push(key);
    }

    return arrayParams.join(',');
  };

  return [
    arrayParams,
    toggleArrayParam,
  ];
}