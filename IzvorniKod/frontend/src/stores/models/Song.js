import { decorate, observable } from 'mobx';

import { Item } from './Item';

export class Song extends Item {
  artists;
  songwriters;
  producers;

  constructor(item) {
    super(item);

    this.artists = item.artists.data || [];
    this.publishers = item.publishers.data || [];
    this.songwriters = item.songwriters.data || [];
    this.producers = item.producers.data || [];
  }

  get info() {
    let strings = [];

    if (this.publishers.length) {
      const publisherNames = this.publishers.map((publisher) => publisher.name);
      strings.push(`Izdavači: ${publisherNames.join(', ')}`);
    }

    if (this.artists.length) {
      const artistNames = this.artists.map((artist) => artist.name);
      strings.push(`Umjetnici: ${artistNames.join(', ')}`);
    }

    if (this.songwriters.length) {
      const songwriterNames = this.songwriters.map((songwriter) => songwriter.name);
      strings.push(`Tekstopisci: ${songwriterNames.join(', ')}`);
    }

    if (this.producers.length) {
      const producerNames = this.producers.map((producer) => producer.name);
      strings.push(`Producenti: ${producerNames.join(', ')}`);
    }

    return strings.join('; ');
  }

  get formattedInfo() {
    let strings = [];

    if (this.publishers.length) {
      const publisherNames = this.publishers.map((publisher) => publisher.name);
      strings.push(`Izdavači: ${publisherNames.join(', ')}`);
    }

    if (this.artists.length) {
      const artistNames = this.artists.map((artist) => artist.name);
      strings.push(`Umjetnici: ${artistNames.join(', ')}`);
    }

    if (this.songwriters.length) {
      const songwriterNames = this.songwriters.map((songwriter) => songwriter.name);
      strings.push(`Tekstopisci: ${songwriterNames.join(', ')}`);
    }

    if (this.producers.length) {
      const producerNames = this.producers.map((producer) => producer.name);
      strings.push(`Producenti: ${producerNames.join(', ')}`);
    }

    return strings.join(`\r\n`);
  }
}

decorate(Song, {
  artists: observable,
  songwriters: observable,
  producers: observable,
})
