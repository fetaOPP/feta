import { decorate, observable } from 'mobx';

import { Item } from './Item';

export class Film extends Item {
  directors;
  producers;
  
  constructor(item) {
    super(item);

    this.directors = item.directors.data || [];
    this.producers = item.producers.data || [];
  }

  get info() {
    let strings = [];

    if (this.directors.length) {
      const directorNames = this.directors.map((director) => director.name);
      strings.push(`Redatelji: ${directorNames.join(',')}`);
    }

    if (this.publishers.length) {
      const publisherNames = this.publishers.map((publisher) => publisher.name);
      strings.push(`Izdavači: ${publisherNames.join(',')}`);
    }

    if (this.producers.length) {
      const producerNames = this.producers.map((producer) => producer.name);
      strings.push(`Producenti: ${producerNames.join(',')}`);
    }

    return strings.join('; ');
  }

  get formattedInfo() {
    let strings = [];

    if (this.directors.length) {
      const directorNames = this.directors.map((director) => director.name);
      strings.push(`Redatelji: ${directorNames.join(', ')}`);
    }

    if (this.publishers.length) {
      const publisherNames = this.publishers.map((publisher) => publisher.name);
      strings.push(`Izdavači: ${publisherNames.join(', ')}`);
    }

    if (this.producers.length) {
      const producerNames = this.producers.map((producer) => producer.name);
      strings.push(`Producenti: ${producerNames.join(', ')}`);
    }

    return strings.join(`\r\n`);
  }
}

decorate(Film, {
  directors: observable,
  producers: observable,
});
