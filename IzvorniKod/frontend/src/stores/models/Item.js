import { decorate, observable } from 'mobx';

import { formatPrice } from '../../services/product';

export class Item {
  id;
  type;
  title;
  description;
  yearOfIssue;
  purchasePrice;
  iconUrl;
  fileUrl;
  available;
  publishers = [];

  constructor(item) {
    this.id = item.id;
    this.type = item.type;
    this.title = item.title;
    this.description = item.description;
    this.yearOfIssue = item.yearOfIssue;
    this.purchasePrice = item.purchasePrice;
    this.iconUrl = item.iconUrl;
    this.fileUrl = item.fileUrl;
    this.available = item.available;

    if (item.publishers && item.publishers.data) {
      this.publishers = item.publishers.data;
    } else if (item.publishers) {
      this.publishers = item.publishers;
    }
  }

  get info() {
    return '';
  }

  get formattedPurchasePrice() {
    return formatPrice(this.purchasePrice);
  }
}

decorate(Item, {
  id: observable,
  type: observable,
  title: observable,
  description: observable,
  yearOfIssue: observable,
  purchasePrice: observable,
  image: observable,
  available: observable,
  publishers: observable,
});
