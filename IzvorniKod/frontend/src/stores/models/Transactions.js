import { createCategoryItem } from "../../services/product";
import { decorate, observable } from "mobx";
import {formatDistanceToNow} from 'date-fns';
import { hr } from 'date-fns/locale';

export class Transaction {
  id;
  item;
  type;
  validUntil;

  constructor(transaction) {
    this.id = transaction.id;
    this.item = createCategoryItem(transaction.item.data ? transaction.item.data : transaction.item);
    this.type = transaction.type;
    this.validUntil = transaction.validUntil;
  }

  get isBought() {
    return this.type === 'PURCHASE';
  }

  get isRented() {
    return !this.isBought;
  }

  get daysUntilInvalid() {
    return formatDistanceToNow(new Date(this.validUntil), { addSuffix: true, locale: hr });
  }
}

decorate(Transaction, {
  id: observable,
  item: observable,
  type: observable,
});
