import { decorate, observable } from 'mobx';

import { Item } from './Item';

export class Book extends Item {
  authors;

  constructor(item) {
    super(item);

    this.authors = item.authors.data || [];
  }

  get info() {
    let strings = [];

    if (this.publishers.length) {
      const publisherNames = this.publishers.map((publisher) => publisher.name);
      strings.push(`Izdavači: ${publisherNames.join(', ')}`);
    }

    if (this.authors.length) {
      const authorNames = this.authors.map((author) => author.name);
      strings.push(`Autori: ${authorNames.join(', ')}`);
    }

    return strings.join('; ');
  }

  get formattedInfo() {
    let strings = [];

    if (this.publishers.length) {
      const publisherNames = this.publishers.map((publisher) => publisher.name);
      strings.push(`Izdavači: ${publisherNames.join(', ')}`);
    }

    if (this.authors.length) {
      const authorNames = this.authors.map((author) => author.name);
      strings.push(`Autori: ${authorNames.join(', ')}`);
    }

    return strings.join(`\r\n`);
  }
}

decorate(Book, {
  authors: observable,
});
