import { Item } from "./Item";
import { decorate, observable } from "mobx";

export class Magazine extends Item {
  issueNumber;
  monthOfIssue;

  constructor(item) {
    super(item);

    this.issueNumber = item.issueNumber;
    this.monthOfIssue = item.monthOfIssue;
  }

  get info() {
    let strings = [];

    if (this.publishers.length) {
      const publisherNames = this.publishers.map((publisher) => publisher.name);
      strings.push(`Izdavači: ${publisherNames.join(', ')}`);
    }

    strings.push(`Izdanje ${this.issueNumber}/${this.monthOfIssue}`);

    return strings.join('; ');
  }

  get formattedInfo() {
    let strings = [];

    if (this.publishers.length) {
      const publisherNames = this.publishers.map((publisher) => publisher.name);
      strings.push(`Izdavači: ${publisherNames.join(', ')}`);
    }

    strings.push(`Izdanje ${this.issueNumber}/${this.monthOfIssue}`);

    return strings.join(`\r\n`);
  }
}

decorate(Magazine, {
  issueNumber: observable,
  monthOfIssue: observable,
});
