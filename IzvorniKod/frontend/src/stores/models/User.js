import { decorate, observable } from 'mobx';
import { Transaction } from './Transactions';

export class User {
  id;
  username;
  role;
  payPalMail;
  transactions = [];

  constructor(user) {
    this.id = user.id;
    this.username = user.username;
    this.role = user.role;
    this.payPalMail = user.payPalMail;

    if (user.transactions && user.transactions.data) {
      this.transactions = user.transactions.data.map((transaction) => new Transaction(transaction));
    } else if (Array.isArray(user.transactions)) {
      this.transactions = user.transactions.map((transaction) => new Transaction(transaction));
    }
  }

  get canAccessAdmin() {
    return this.isAdmin || this.isCoordinator;
  }

  get isAdmin() {
    return this.role === 'ADMIN';
  }

  get isCoordinator() {
    return this.role === 'COORDINATOR';
  }

  transactionForItem(itemId) {
    return this.transactions.find((transaction) => transaction.item.id === Number(itemId));
  }

  get boughtTransactions() {
    return this.transactions.filter((t) => t.isBought);
  }

  get rentedTransactions() {
    return this.transactions.filter((t) => t.isRented);
  }
}

decorate(User, {
  id: observable,
  username: observable,
  role: observable,
  payPalMail: observable,
});
