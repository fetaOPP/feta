import { observable, decorate, computed } from 'mobx';

export class AppStore {
  user;

  get isLoggedIn() {
    return Boolean(this.user);
  }
}

decorate(AppStore, {
  user: observable,
  isLoggedIn: computed,
});
