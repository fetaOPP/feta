import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import 'antd/dist/antd.css';
import './index.css';

import logotip from './static/logotip.png';

import { Navigation } from './components/navigation/Navigation';
import { Home } from './pages/Home';
import { Login } from './pages/Login';
import { Register } from './pages/Register';
import { Search } from './pages/Search';
import { NotFound } from './pages/NotFound';
import { fetchSession } from './services/session';
import { Provider, observer } from 'mobx-react';
import { AppStore } from './stores/AppStore';
import { Product } from './pages/Product';
import { Admin } from './pages/Admin';
import { Transactions } from './pages/Transactions';
import { NewItem } from './pages/NewItem';
import { EditItem } from './pages/EditItem';

class AppC extends Component {
  appStore;

  constructor(props) {
    super(props);

    this.appStore = new AppStore();
  }

  async componentDidMount() {
    await fetchSession();
  }

  render() {
    return (
      <Provider
        appStore={this.appStore}
      >
        <head>
          <link rel="shortcut icon" type="image/png" href={logotip} />
        </head>

        <Router>
          <div className="wrapper">
            <Navigation />
            <Switch>
              <Route exact path="/products/:id" component={Product} />
              <Route exact path="/search" component={Search} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/admin" component={Admin} />
              <Route exact path="/admin/content/new" component={NewItem} />
              <Route exact path="/admin/content/:id" component={EditItem} />
              <Route exact path="/admin/user/:id" component={Transactions} />
              <Route exact path="/" component={Home} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export const App = observer(AppC);
