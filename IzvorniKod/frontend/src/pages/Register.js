import React from 'react';
import { useHistory } from 'react-router-dom';

import { RegistrationForm } from '../components/forms/RegistrationForm';
import { Panel } from '../components/core/Panel';
import { register } from '../services/session';
import { notify } from '../utils/notifications';

export function Register() {
  const history = useHistory();

  const handleSubmit = async (payload, actions) => {
    try {
      await register(payload);
      notify('success', 'Dobrodošli na mediateku', 'Sada se možete prijaviti');
      history.push('/login');
    } catch (response) {
      if (response) {
        actions.setErrors(response);
      } else {
        notify('error', 'Nešto ', 'Molimo, pokušajte ponovno');
      }
    }
  }

  return (
    <Panel>
      <RegistrationForm
        onSubmit={handleSubmit}
      />
    </Panel>
  );
}
