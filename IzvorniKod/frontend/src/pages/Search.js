import { Row, Col, Spin } from 'antd';
import React, { Fragment } from 'react';

import { SearchBox } from '../components/SearchBox';
import { Margin } from '../components/core/Margin';
import { useSearch } from '../utils/useSearch';
import { SearchFilters } from '../components/search/SearchFilters';
import { SearchResults } from '../components/search/SearchResults';

export function Search() {
  const {items, loading, params, updateParams, totalCount} = useSearch({
    available: 'true',
  });

  const handleSearch = (newQuery) => {
    updateParams('query', newQuery);
  }

  return (
    <Fragment>
      <Margin margin="0 0 48px">
        <SearchBox
          enterButton="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PRETRAŽI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
          defaultValue={params.get('query')}
          onSearch={handleSearch}
        />
      </Margin>
      <Row gutter={32}>
        <Col span={6}>
          <SearchFilters
            params={params}
            updateParams={updateParams}
          />
        </Col>
        <Col span={18}>
          {loading ? (
            <div style={{ textAlign: 'center' }}>
              <Spin size="large" />   
            </div>
          ) : (
            <SearchResults
              products={items}
              totalCount={totalCount}
              params={params}
              updateParams={updateParams}
            />
          )}
        </Col>
      </Row>
    </Fragment>
  );
}
