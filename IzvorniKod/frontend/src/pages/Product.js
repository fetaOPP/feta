import { Row, Col } from 'antd';
import React, { Component } from 'react';

import { ProductImage } from '../components/product/ProductImage';
import { ProductHeader } from '../components/product/ProductHeader';
import { Margin } from '../components/core/Margin';
import { fetchItem } from '../services/item';
import { decorate, observable } from 'mobx';
import { observer } from 'mobx-react';
import { ProductSuggestions } from '../components/product/ProductSuggestions';

class ProductC extends Component {
  item;

  async componentDidMount() {
    const productId = this.props.match.params.id;

    this.item = await fetchItem(productId);
  }

  async componentDidUpdate(prevProps) {
    const productId = this.props.match.params.id;
    const prevProductId = prevProps.match.params.id;

    if (productId !== prevProductId) {
      this.item = await fetchItem(productId);
    }
  }

  render() {
    return this.item ? (
      <div>
        <Margin margin="0 0 64px">
          <Row gutter={64}>
            <Col span={5}>
              <ProductImage
                src={this.item.iconUrl}
              />
            </Col>
            <Col span={19}>
              <ProductHeader
                product={this.item}
              />
            </Col>
          </Row>
        </Margin>
        <ProductSuggestions
          type={this.item.type}
        />
      </div>
    ) : null;
  }
}

decorate(ProductC, {
  item: observable,
});

export const Product = observer(ProductC);
