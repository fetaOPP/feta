import React, { Fragment } from 'react';
import {Divider} from 'antd';
import { inject, observer } from 'mobx-react';
import { UpdateUserInfoForm } from '../components/forms/UpdateUserInfoForm';
import { ChangePasswordForm } from '../components/forms/ChangePasswordForm';
import { notify } from '../utils/notifications';
import { MaxWidth } from '../components/core/MaxWidth';
import { Margin } from '../components/core/Margin';

function PreferencesC() {
  const onInfoSubmit = async (payload, actions) => {
    try {
      notify('success', 'Uspješno ste ažurirali svoje podatke👏🏼');
    } catch (response) {
      if (response) {
        actions.setErrors(response);
      }
    }
  };

  const onNewPasswordSubmit = async (payload, actions) => {
    try {
      notify('success', 'Uspješno ste promijenili lozinku');
    } catch (response) {
      if (response) {
        actions.setErrors(response);
      }
    }
  };
  
  return (
    <Fragment>
      <MaxWidth maxWidth="300px">
        <UpdateUserInfoForm
          onSubmit={onInfoSubmit}
        />
      </MaxWidth>

      <Margin margin="32px 0 0">
        <Divider />
      </Margin>

      <MaxWidth maxWidth="300px">
        <ChangePasswordForm
          onSubmit={onNewPasswordSubmit}
        />
      </MaxWidth>
    </Fragment>
  );
}

export const Preferences = inject('appStore')(observer(PreferencesC));
