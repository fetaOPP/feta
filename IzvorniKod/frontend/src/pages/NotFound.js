import React from 'react';
import { Typography } from 'antd';

import { Panel } from '../components/core/Panel';
import feta from '../static/feta.png';

export function NotFound() {
  return (
    <Panel>
      <Typography.Title style={{ color: '#1890ff', textAlign: 'center' }} level={2}>
        Ovo nije feta koju tražiš.
      </Typography.Title> 
      <img
        src={feta}
        style={{ maxWidth: '100%' }}
        alt="feta cheese"
      />
    </Panel>
  );
}
