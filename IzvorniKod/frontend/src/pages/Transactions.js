import { Radio, Icon, Typography, Spin } from 'antd';
import { observer, inject } from 'mobx-react';
import React, { Component } from 'react';

import { Margin } from '../components/core/Margin';
import { UserItem } from '../components/UserItem';
import { fetchUser } from '../services/user';
import { decorate, observable } from 'mobx';

class TransactionsC extends Component {
  loading = true;
  user;
  category = 'rented';
  transactions = [];

  constructor(props) {
    super(props);

    this.handleCategoryChange = this.handleCategoryChange.bind(this);
  }

  async componentDidMount() {
    const userId = this.props.match.params.id;

    this.loading = true;

    if (this.props.appStore.user.id === Number(userId)) {
      this.user = this.props.appStore.user;
    } else {
      this.user = await fetchUser(userId);
    }

    this.loading = false;

    this.updateTransactions();
  }

  handleCategoryChange(event) {
    this.category = event.target.value;
    this.updateTransactions();
  }

  updateTransactions() {
    if (this.category === 'rented') {
      this.transactions = this.user.rentedTransactions;
    } else {
      this.transactions = this.user.boughtTransactions;
    }
  }

  render() {
    return (
      <div>
        <Margin margin="0 0 48px">
        <Radio.Group onChange={this.handleCategoryChange} value={this.category}>
            <Radio.Button value="rented">Posuđeni</Radio.Button>
            <Radio.Button value="bought">Kupljeni</Radio.Button>
        </Radio.Group>
        </Margin>
        {this.loading ? (
          <div style={{ textAlign: 'center' }}>
            <Spin size="large" />   
          </div>
        ) : this.transactions.length ? (
          this.transactions.map((transaction) => (
            <UserItem
              key={transaction.id}
              product={transaction.item}
              smallImage
              footer={transaction.isRented ? (
                <div>
                  <Icon type="clock-circle" />&nbsp;&nbsp;
                  <Typography.Text>
                    Posudba ističe {transaction.daysUntilInvalid}
                  </Typography.Text>
                </div>
              ) : null}
            />
          ))
        ) : (
          <div>
            Nema {this.category === 'rented' ? 'posuđenog' : 'kupljenog'} sadržaja
          </div>
        )}
      </div>
    );
  }
}

decorate(TransactionsC, {
  loading: observable,
  user: observable,
  category: observable,
  transactions: observable,
});

export const Transactions = inject('appStore')(observer(TransactionsC));
