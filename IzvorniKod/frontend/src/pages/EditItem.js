import { inject, observer } from 'mobx-react';
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { ItemForm } from '../components/forms/ItemForm'
import { fetchItem, createItem } from '../services/item';
import { notify } from '../utils/notifications';

function EditItemC(props) {
  const history = useHistory();

  if (!props.appStore.user) {
    history.push('/login');
  }

  if (!props.appStore.user.canAccessAdmin) {
    history.push('/');
  }

  const [loading, setLoading] = useState(true);
  const [item, setItem] = useState(null);

  const handleFetchItem = async () => {
    setLoading(true);

    const fetchedItem = await fetchItem(props.match.params.id);
    setItem(fetchedItem);

    setLoading(false);
  };

  useEffect(() => {
    handleFetchItem();
    return () => {};
  }, []);

  const handleSubmit = async (payload) => {
    try {
      await createItem(payload);
    } catch {
      notify('error', 'Greška kod ažuriranja proizvoda', 'Molimo, pokušajte ponovno');
      return;
    }
    history.push(`/admin?category=content`);
    notify('success', 'Uspješno ste ažurirali proizvod');
  }

  return !loading ? (
    <ItemForm
      title="Uredi proizvod"
      item={item}
      onSubmit={handleSubmit}
    />
  ) : null;
}

export const EditItem = inject('appStore')(observer(EditItemC));
