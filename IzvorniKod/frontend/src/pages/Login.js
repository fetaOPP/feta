import { inject, observer } from 'mobx-react';
import React from 'react';
import { useHistory } from 'react-router-dom';

import { LoginForm}  from '../components/forms/LoginForm';
import { Panel } from '../components/core/Panel';
import { login } from '../services/session';
import { notify } from '../utils/notifications';

function LoginC(props) {
  const history = useHistory();

  const onSubmit = async (payload, actions) => {
    try {
      await login(props.appStore, payload);
      notify('success', 'Uspješno ste prijavljeni');
      history.push('/');
    } catch (response) {
      if (response) {
        actions.setErrors(response);
      }
    }
  };

  return (
    <Panel>
      <LoginForm
        onSubmit={onSubmit}
      />
    </Panel>
  );
}

export const Login = inject('appStore')(observer(LoginC));
