import { Radio } from 'antd';
import React from 'react';
import { Redirect } from 'react-router-dom';

import { useQuery } from '../utils/useQuery';
import { Margin } from '../components/core/Margin';
import { AdminUsers } from '../components/admin/AdminUsers';
import { AdminContent } from '../components/adminContent/AdminContent';
import { inject, observer } from 'mobx-react';

function AdminC({ appStore }) {
  const [params, setParams] = useQuery();

  const category = params.get('category') || 'content';

  const setCategory = (e) => setParams('category', e.target.value);

  return appStore.user ? (
    <div>
      {appStore.user.isAdmin ? (
        <Margin margin="0 0 48px">
          <Radio.Group onChange={setCategory} value={category}>
            <Radio.Button value="content">Sadržaj</Radio.Button>
            <Radio.Button value="users">Korisnici</Radio.Button>
          </Radio.Group>
        </Margin>
      ) : null}

      {category === 'content' ? (
        <AdminContent />
      ) : (
        <AdminUsers />
      )}
    </div>
  ) : (
    <Redirect
      to="/login"
    />
  );
}

export const Admin = inject('appStore')(observer(AdminC));
