import { PageHeader, Typography, Radio } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import { Margin } from '../components/core/Margin';
import { useQuery } from '../utils/useQuery';
import { SearchFilters } from '../components/adminUser/SearchFilters';
import { SearchResults } from '../components/adminUser/SearchResults';

const user = {
  username: 'Hrco',
};

const products = [
  {
    title: 'Avengers: Endgame',
    image: 'https://i.imgur.com/JCa4yrd.jpg',
    description: 'After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos’ actions and restore balance to the universe.',
    purchasePrice: 100,
    year: 2019,
  },
  { 
    title: 'The Godfather', 
    image: 'https://i.imgur.com/HzWqgT1.jpg',
    description: 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.',
    purchasePrice: 40,
    year: 1972,
  },
  {
    title: 'The Royal Tenenbaums',
    image: 'https://i.imgur.com/JaBFmpO.jpg',
    description: 'After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos’ actions and restore balance to the universe.',
    purchasePrice: 100,
    year: 2019,
  },
  {
    title: 'La Notte',
    image: 'https://i.imgur.com/U4tFHnk.jpg',
    description: 'After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos’ actions and restore balance to the universe.',
    purchasePrice: 100,
    year: 2019,
  },
  {
    title: 'Alien',
    image: 'https://i.imgur.com/7EGv7ZN.jpg',
    description: 'After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos’ actions and restore balance to the universe.',
    purchasePrice: 100,
    year: 2019,
  },
];

export function AdminUser() {
  const [params, setParams] = useQuery();

  const category = params.get('category') || 'rented';

  const setCategory = (e) => setParams('category', e.target.value);

  return (
    <div>
      <Margin margin="0 0 48px">
        <Link
          to="/admin?category=users"
        >
          <PageHeader
            onBack={() => null}
            title="Vrati se na pregled korisnika"
            style={{ padding: '0' }}
          />
        </Link>
      </Margin>
      <Margin margin="0 0 24px">
        <Typography.Text style={{ fontSize: '18px', color: 'black' }}>
          Korisnik:&nbsp;
        </Typography.Text>
        <Typography.Text strong style={{ fontSize: '18px', color: 'black' }}>
          {user.username}
        </Typography.Text>
      </Margin>

      <Margin margin="0 0 24px">
        <Radio.Group onChange={setCategory} value={category}>
          <Radio.Button value="rented">Posuđeno</Radio.Button>
          <Radio.Button value="bought">Kupljeno</Radio.Button>
        </Radio.Group>
      </Margin>

      <Margin margin="0 0 32px">
        <SearchFilters />
      </Margin>

      <SearchResults products={products} />
    </div>
  );
}
