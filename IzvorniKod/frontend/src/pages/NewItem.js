import React from 'react';
import { useHistory } from 'react-router-dom';

import { ItemForm } from '../components/forms/ItemForm';
import { createItem } from '../services/item';
import { notify } from '../utils/notifications';
import { inject, observer } from 'mobx-react';

function NewItemC({ appStore }) {
  const history = useHistory();

  if (!appStore.user) {
    history.push('/login');
  }

  if (!appStore.user.canAccessAdmin) {
    history.push('/');
  }

  const handleSubmit = async (payload) => {
    try {
      await createItem(payload);
    } catch {
      notify('error', 'Greška kod stvaranja proizvoda', 'Molimo, pokušajte ponovno');
      return;
    }
    history.push(`/admin?category=content`);
    notify('success', 'Uspješno ste stvorili proizvod');
  }

  return (
    <ItemForm
      title="Novi proizvod"
      onSubmit={handleSubmit}
    />
  );
}

export const NewItem = inject('appStore')(observer(NewItemC));
