import React, { Fragment } from 'react';

import { HomeSearch } from '../components/home/HomeSearch';
import { ProductCarousel } from '../components/ProductCarousel/ProductCarousel';
import { Margin } from '../components/core/Margin';
import { useCarousel } from '../services/pages/home';

export function Home() {
  const [songs, songsLoading] = useCarousel('song');
  const [films, filmsLoading] = useCarousel('film');
  const [books, booksLoading] = useCarousel('book');
  const [magazines, magazinesLoading] = useCarousel('magazine');

  return (
    <Fragment>
      <Margin
        margin="0 0 160px"
      >
        <HomeSearch />
      </Margin>
      <Margin margin="0 0 40px">
        <ProductCarousel
          title="Glazba"
          products={songs}
          loading={songsLoading}
        />
      </Margin>
      <Margin margin="0 0 40px">
        <ProductCarousel
          title="Filmovi"
          products={films}
          loading={filmsLoading}
        />
      </Margin>
      <Margin margin="0 0 40px">
        <ProductCarousel
          title="Knjige"
          products={books}
          loading={booksLoading}
        />
      </Margin>
      <ProductCarousel
        title="Časopisi"
        products={magazines}
        loading={magazinesLoading}
      />
    </Fragment> 
  );
}