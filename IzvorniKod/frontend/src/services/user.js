import { request } from "./api";
import { User } from "../stores/models/User";
import { useState, useEffect } from "react";
import { useQuery } from "../utils/useQuery";

export const userInclude = [
  'transactions.item.publishers',
  'transactions.item.directors',
  'transactions.item.producers',
  'transactions.item.authors',
  'transactions.item.artists',
  'transactions.item.songwriters',
].join(',');

export function useUserSearch(initialParams = {}) {
  const [totalCount, setTotalCount] = useState(0);
  const [params, updateParams] = useQuery();
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const onFetch = async () => {
    setLoading(true);

    const items = await fetchUsers(params);
    setItems(items.data.map((item) => new User(item)));
    setTotalCount(items.meta.totalResourceCount);

    setLoading(false);
  }

  useEffect(() => {
    Object.keys(initialParams).forEach((key) => updateParams(key, initialParams[key]));
  }, []);

  useEffect(() => {
    onFetch();
    return () => {};
  }, [params.toString()]);

  return {
    items,
    loading,
    params,
    updateParams,
    totalCount,
    onFetch,
  };
}

export async function fetchUsers(params) {
  const queryParams = new URLSearchParams();

  const pageSize = params.get('size') || 5;

  queryParams.set('page[size]', pageSize);
  
  if (params.has('page')) {
    queryParams.set('page[number]', params.get('page'));
  }

  if (params.has('include')) {
    queryParams.set('include', params.get('include'));
  }

  return request(`api/mediatekaUser?${queryParams.toString()}`, 'GET');
}

export async function fetchUser(id) {
  const queryParams = new URLSearchParams();

  queryParams.set('include', userInclude);

  const response = await request(`api/mediatekaUser/${id}?${queryParams.toString()}`, 'GET');

  return new User(response.data);
}

export async function updateUserRole(user, role) {
  await request(`api/mediatekaUser/${user.id}`, 'PATCH', {
    data: {
      id: user.id,
      type: "mediatekaUser",
      role,
    }
  });

  user.role = role;
}

export async function deleteUser(user) {
  return request(`api/mediatekaUser/${user.id}`, 'DELETE');
}
