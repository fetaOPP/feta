import { useState, useEffect } from 'react';
import { fetchItems } from '../product';

export function useCarousel(type) {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleFetchItems = async () => {
    setLoading(true);

    const items = await fetchItems(new URLSearchParams({ type, available: 'true' }));
    setItems(items.data);

    setLoading(false);
  };

  useEffect(() => {
    handleFetchItems();
    return () => {};
  }, []);

  return [
    items,
    loading,
  ];
}
