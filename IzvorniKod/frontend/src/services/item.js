import { request } from './api';
import { createCategoryItem } from './product';

export function createItem(payload) {
  const newPayload = { ...payload };

  Object.keys(newPayload).forEach((key) => {
    if (!newPayload[key]) {
      delete newPayload[key];
    }
  })

  return request('resource/uploadItem', 'POST', newPayload);
}

export async function fetchItem(id) {
  const response = await request(`api/item/${id}?include=publishers,authors,artists,songwriters,producers,directors`, 'GET');
  return createCategoryItem(response.data);
}
