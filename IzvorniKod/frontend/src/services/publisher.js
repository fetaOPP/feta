import { request } from './api';

export async function fetchPublishers(query) {
  const queryParams = new URLSearchParams();

  queryParams.set('filter[name][LIKE]', `%${query}%`);

  const response = await request(`api/publisher?${queryParams.toString()}`, 'GET');

  return response.data;
}
