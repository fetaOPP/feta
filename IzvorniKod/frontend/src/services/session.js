import { request } from './api';
import { User } from '../stores/models/User';

export async function fetchSession(appStore) {
  try {
    const user = await request('session', 'GET');
    appStore.user = new User(user);
  } catch {}
}

export async function login(appStore, payload) {
  const user = await request('session', 'POST', payload);
  appStore.user = new User(user);
}

export async function logout(appStore) {
  await request('logout', 'GET');
  appStore.user = null;
}

export async function register(payload) {
  return request('register', 'POST', payload);
}
