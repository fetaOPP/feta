const BASE_URL = 'https://mediateka-api.herokuapp.com/';

export async function request(endpoint, method, body) {
  const response = await fetch(`${BASE_URL}${endpoint}`,{
    method,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    credentials: 'include',
    body: body ? JSON.stringify(body) : null,
  });

  const contentType = response.headers.get('content-type');
  const isJson = contentType && contentType.indexOf("application/json") !== -1;

  if (isJson) {
    const json = await response.json();
    if (response.ok) {
      return json;
    } else {
      throw json;
    }
  } else if (!response.ok) {
    throw response;
  }
}

export async function uploadThumbnail(file) {
  const response = await fetch(`${BASE_URL}resource/thumbnailPutUrl?name=${file.name}`, {
    credentials: 'include',
  });

  const responseBody = await response.json();

  return responseBody.link;
}

export async function uploadFile(file) {
  const response = await fetch(`${BASE_URL}resource/itemPutUrl?name=${file.name}&type=${file.type}`, {
    credentials: 'include',
  });

  const responseBody = await response.json();

  return responseBody.link;
}
