import { request } from './api';
import { Book } from '../stores/models/Book';
import { Item } from '../stores/models/Item';
import { Song } from '../stores/models/Song';
import { Film } from '../stores/models/Film';
import { Magazine } from '../stores/models/Magazine';

export function calculateRentPrice(purchasePrice, interval) {
  switch (interval) {
    case '24_hours': return purchasePrice * 0.2;
    case '48_hours': return purchasePrice * 0.4;
    case '1_week': return purchasePrice * 0.6;
    case '1_month': return purchasePrice * 0.8;
    default: return purchasePrice;
  }
}

export function formatPrice(price) {
  return `${price} HRK`;
}

export function createCategoryItem(item) {
  switch (item.type) {
    case 'book': return new Book(item);
    case 'song': return new Song(item);
    case 'film': return new Film(item);
    case 'magazine': return new Magazine(item);
    default: return new Item(item);
  }
}

export function fetchItems(params) {
  const queryParams = new URLSearchParams();

  const pageSize = params.get('size') || 5;

  queryParams.set('page[size]', pageSize);
  
  if (params.has('page')) {
    queryParams.set('page[number]', params.get('page'));
  }
  
  if (params.has('type')) {
    queryParams.set('filter[type]', params.get('type'));
  }
  
  if (params.has('query')) {
    queryParams.set('filter[title][LIKE]', `%${params.get('query')}%`);
  }

  if (params.has('minPrice')) {
    queryParams.set('filter[purchasePrice][GE]', params.get('minPrice'));
  }

  if (params.has('maxPrice')) {
    queryParams.set('filter[purchasePrice][LE]', params.get('maxPrice'));
  }

  if (params.has('include')) {
    queryParams.set('include', params.get('include'));
  }

  if (params.has('available')) {
    queryParams.set('filter[available][EQ]', params.get('available'));
  }
  
  return request(`api/item?${queryParams.toString()}`, 'GET');
}
