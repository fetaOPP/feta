import { request } from './api';

function intervalToApiInterval(interval) {
  switch (interval) {
    case '24_hours': return 'DAY';
    case '48_hours': return 'TWO_DAY';
    case '1_week': return 'WEEK';
    case '1_month': return 'MONTH';
    default: return 'BUY';
  }
}

export function createPurchase(itemId, interval = '') {
  const params = new URLSearchParams({
    itemId,
    purchaseType: intervalToApiInterval(interval),
  });

  return request(`purchase?${params.toString()}`, 'GET');
}
