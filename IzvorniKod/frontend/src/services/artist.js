import { request } from './api';

export async function fetchArtists(query) {
  const queryParams = new URLSearchParams();

  queryParams.set('filter[name][LIKE]', `%${query}%`);

  const response = await request(`api/artist?${queryParams.toString()}`, 'GET');

  return response.data;
}
