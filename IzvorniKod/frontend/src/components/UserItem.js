import { Row, Col, Divider, Typography } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

import { ProductImage } from './product/ProductImage';
import { Margin } from './core/Margin';

export function UserItem({ product, footer, smallImage }) {
  return (
    <Margin margin="0 0 32px">
      <Row gutter={48}>
        <Col span={smallImage ? 4 : 5}>
          <Link to={`/products/${product.id}`}>
            <ProductImage src={product.iconUrl} />
          </Link>
        </Col>
        <Col span={smallImage ? 20 : 19}>
          <Margin margin="0 0 24px">
            <Margin margin="0 0 -8px">
              <Link to={`/products/${product.id}`}>
                <Typography.Title level={4}>
                  {product.title}
                </Typography.Title>
              </Link>
            </Margin>
            <Typography.Text>
              {product.yearOfIssue}
            </Typography.Text>
          </Margin>

          <Margin margin="0 0 12px">
            <Typography.Text>
              {product.description}
            </Typography.Text>
          </Margin>

          <Typography.Text style={{ whiteSpace: 'pre-line' }}>
            {product.formattedInfo}
          </Typography.Text>

          {footer && (
            <Margin margin="24px 0 0">
              {footer}
            </Margin>
          )}
        </Col>
      </Row>
      <Margin margin="32px 0 0">
        <Divider />
      </Margin>
    </Margin>
  );
}
