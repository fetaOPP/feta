import React from 'react';

export function MaxWidth({ maxWidth = '0px', children }) {
  return (
    <div
      style={{
        maxWidth,
      }}
    >
      {children}
    </div>
  )
}
