import { Select, Spin } from 'antd';
import React, { useState } from 'react';

import { fetchPublishers } from '../../services/publisher';

export function PublisherSelect({ name, formik, ...props }) {
  const [data, setData] = useState([]);
  const [fetching, setFetching] = useState(false);

  const handleSearch = async (query) => {
    setFetching(true);

    const publishers = await fetchPublishers(query);
    setData(publishers);
    
    setFetching(false);
  };

  const handleChange = (values) => {
    const newValues = values.filter((value) => !Number(value));
    const existingValues = values.filter((value) => Number(value));

    if (newValues.join(',')) {
      formik.setFieldValue(name, newValues.join(','));
    }

    formik.setFieldValue(`${name}Ids`, existingValues);
  };

  const formattedValue = (formik.values[`${name}Ids`] || []).concat(formik.values[name] ? formik.values[name].split(',') : []);

  return (
    <Select
      mode="tags"
      style={{ width: '100%' }}
      notFoundContent={fetching ? <Spin size="small" /> : null}
      filterOption={false}
      value={formattedValue}
      onChange={handleChange}
      onSearch={handleSearch}
      {...props}
    >
      {data.map((d) => (
        <Select.Option key={Number(d.id)}>{d.name}</Select.Option>
      ))}
    </Select>
  );
}
