import React from 'react';

export function Panel(props) {
  return (
    <div
      style={{
        maxWidth: props.maxWidth || '400px',
        margin: '0 auto',
        textAlign: props.center && 'center',
        border: props.border,
        borderRadius: props.borderRadius,
        padding: props.padding || '0',
      }}
    >
      {props.children}
    </div>
  );
}
