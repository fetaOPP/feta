import React from 'react';
import { Input } from 'antd';

export function SearchBox(props) {
  return (
    <Input.Search
      enterButton
      placeholder="Pretraži knjige, filmove, časopise, glazbu..."
      size="large"
      {...props}
    />
  );
}
