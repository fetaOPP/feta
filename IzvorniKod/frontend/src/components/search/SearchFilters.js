import { Typography, Row, Col, Checkbox, InputNumber } from 'antd';
import React, { Fragment } from 'react';

import { Margin } from '../core/Margin';
import { useArrayParams } from '../../utils/useArrayParams';

export function SearchFilters({ params, updateParams }) {
  const [types, toggleType] = useArrayParams('type');

  const handleCategoryUpdate = (event) => {
    const type = event.target.value;
    updateParams('type', toggleType(type));
  }

  const updateMinPrice = (value) => updatePriceFilter('minPrice', value);
  const updateMaxPrice = (value) => updatePriceFilter('maxPrice', value);

  const updatePriceFilter = (filter, value) => {
    const price = Number(value);
    if (price || price === 0) {
      updateParams(filter, price);
    }
  }

  return (
    <Fragment>
      <Margin margin="0 0 32px">
        <Typography.Title level={4}>Kategorija</Typography.Title>
        <Row gutter={[16, 16]}>
          <Col span={10}>
            <Checkbox
              value="song"
              checked={types.includes('song')}
              onChange={handleCategoryUpdate}
            >
              Glazba
            </Checkbox>
          </Col>
          <Col span={10}>
            <Checkbox
              value="film"
              checked={types.includes('film')}
              onChange={handleCategoryUpdate}
            >
              Film
            </Checkbox>
          </Col>
          <Col span={10}>
            <Checkbox
              value="magazine"
              checked={types.includes('magazine')}
              onChange={handleCategoryUpdate}
            >
              Časopis
            </Checkbox>
          </Col>
          <Col span={10}>
            <Checkbox
              value="book"
              checked={types.includes('book')}
              onChange={handleCategoryUpdate}
            >
              Knjiga
            </Checkbox>
          </Col>
        </Row>
      </Margin>
      <Margin margin="0 0 32px">
        <Typography.Title level={4}>Cijena</Typography.Title>
        <InputNumber
          width="100%"
          onChange={updateMinPrice}
          defaultValue={params.has('minPrice') ? Number(params.get('minPrice')) : undefined}
          placeholder="min"
        />
        <Typography.Text style={{ margin: '0 16px' }}>do</Typography.Text>
        <InputNumber
          width="100%"
          onChange={updateMaxPrice}
          defaultValue={params.has('maxPrice') ? Number(params.get('maxPrice')) : undefined}
          placeholder="max"
        />
      </Margin>
    </Fragment>
  );
}
