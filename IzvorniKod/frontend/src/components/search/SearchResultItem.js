import { Row, Col, Divider, Typography, Button } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

import { ProductImage } from '../product/ProductImage';
import { Margin } from '../core/Margin';
import { formatPrice } from '../../services/product';

export function SearchResultItem({ product, hideBuyButton, footer, smallImage }) {
  return (
    <Margin margin="0 0 32px">
      <Row gutter={48}>
        <Col span={smallImage ? 4 : 5}>
          <Link to={`/products/${product.id}`}>
            <ProductImage src={product.iconUrl} />
          </Link>
        </Col>
        <Col span={smallImage ? 20 : 19}>
          <Margin margin="0 0 24px">
            <Row
              type="flex"
              justify="space-between"
            >
              <div>
                <Margin margin="0 0 -8px">
                  <Link to={`/products/${product.id}`}>
                    <Typography.Title level={4}>
                      {product.title}
                    </Typography.Title>
                  </Link>
                </Margin>
                <Typography.Text>
                  {product.year}
                </Typography.Text>
              </div>
              {!hideBuyButton && (
                <Link to={`/products/${product.id}`}>
                  <Button
                    size="large"
                  >
                    {formatPrice(product.purchasePrice)}
                  </Button>
                </Link>
              )}
            </Row>
          </Margin>

          <Typography.Text >
            {product.description}
          </Typography.Text>

          {footer && (
            <Margin margin="24px 0 0">
              {footer}
            </Margin>
          )}
        </Col>
      </Row>
      <Margin margin="32px 0 0">
        <Divider />
      </Margin>
    </Margin>
  );
}
