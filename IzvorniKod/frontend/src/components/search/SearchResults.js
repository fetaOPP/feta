import { Typography, Pagination, Empty } from 'antd';
import React, { Fragment } from 'react';

import { Margin } from '../core/Margin';
import { SearchResultItem } from './SearchResultItem';

export function SearchResults({ products, totalCount, params, updateParams }) {
  const page = Number(params.get('page') || 1);

  const handlePageChange = (page) => {
    updateParams('page', page);
  }

  return (
    <Fragment>
      {totalCount > 0 ? (
        <Margin margin="0 0 24px">
          <Typography.Text style={{ fontSize: '18px' }}>
            {totalCount} {totalCount === 1 ? 'rezultat' : 'rezultata'}
          </Typography.Text>
        </Margin>
      ) : null}

      {totalCount > 0 ? (
        <Fragment>
          {products.map((product) => (
            <SearchResultItem
              key={product.title}
              product={product}
            />
          ))}
          <Pagination
            total={totalCount}
            pageSize={5}
            current={page}
            onChange={handlePageChange}
            style={{ textAlign: 'center' }}
          />
        </Fragment>
      ) : (
        <Empty
          image={Empty.PRESENTED_IMAGE_DEFAULT}
          description={`Nažalost za ovaj pojam nismo našli nijedan rezultat.\r\nPokušajte promijeniti pojam pretraživanja ili filtere.`}
          style={{ whiteSpace: 'pre-line' }}
        />
      )}
    </Fragment>
  )
}
