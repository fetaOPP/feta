import { Dropdown, Button, Icon } from 'antd';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { useHistory } from 'react-router-dom';

import { NavigationDropdownMenu } from './NavigationDropdownMenu';
import { logout } from '../../services/session';
import { notify } from '../../utils/notifications';

function NavigationDropdownC(props) {
  const history = useHistory();

  const handleLogout = async () => {
    try {
      await logout(props.appStore);
      notify('success', 'Uspješno ste odjavljeni');
      history.push('/');
    } catch {
      notify('error', 'Neuspješna odjava', 'Molimo, pokušajte ponovno');
    }
  };

  return (
    <Dropdown
      overlay={NavigationDropdownMenu(props.user, handleLogout)}
      trigger={['click']}
    >
      <Button>
        {props.user.username} <Icon type="down" />
      </Button>
    </Dropdown>
  );
}

export const NavigationDropdown = inject('appStore')(observer(NavigationDropdownC));
