import { Menu, Icon } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

export function NavigationDropdownMenu(user, onLogout) {
  return (
    <Menu>
      <Menu.Item key="0">
        <Link
          to={`/admin/user/${user.id}`}
        >
         <Icon type="read" /> Moj sadržaj
        </Link>
      </Menu.Item>
      {user.canAccessAdmin ? (
        <Menu.Item key="1">
          <Link
            to="/admin"
          >
          <Icon type="table" /> Admin
          </Link>
        </Menu.Item>
      ) : null}
      <Menu.Item key="3" onClick={onLogout}>
         <Icon type="logout" /> Odjava
      </Menu.Item>
    </Menu>
  );
}
