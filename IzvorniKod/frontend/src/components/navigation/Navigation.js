import { Button } from 'antd';
import { inject, observer } from 'mobx-react';
import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import './Navigation.css';

import { Logo } from '../Logo';
import { NavigationDropdown } from './NavigationDropdown';

function NavigationC(props) {
  return (
    <div className="navigation">
      <Link
        to="/"
      >
        <Logo />
      </Link>
      <div>
        {props.appStore.user ? (
          <NavigationDropdown
            user={props.appStore.user}
          />
        ) : (
          <Fragment>
            <Link
              to="/register"
            >
              <Button
                type="primary"
                style={{
                  marginRight: '10px',
                }}
              >
                Registracija
              </Button>
            </Link>
            <Link
              to="/login"
            >
              <Button>
                Prijava
              </Button>
            </Link>
          </Fragment>
        )}
      </div>
    </div>
  );
}

export const Navigation = inject('appStore')(observer(NavigationC));
