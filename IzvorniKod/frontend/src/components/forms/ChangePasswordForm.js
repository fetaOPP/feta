import { Input, Button, Typography } from 'antd';
import { Formik } from 'formik';
import React from 'react';
import { object, string, ref } from 'yup';

import { ErrorMessage } from '../core/ErrorMessage';
import { Margin } from '../core/Margin';

const validationSchema = object().shape({
  oldPassword: string().required('Morate unijeti trenutnu lozinku'),
  newPassword: string().required('Morate unijeti novu lozinku'),
  confirmPassword: string()
    .oneOf([ref('newPassword'), ''], 'Lozinke se moraju podudarati')
    .required('Morate ponoviti novu lozinku'),
});

const initialValues = {
  oldPassword: '',
  newPassword: '',
  confirmPassword: '',
};

export function ChangePasswordForm(props) {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={props.onSubmit}
      validationSchema={validationSchema}
    >
      {(formik) => (
        <form onSubmit={formik.handleSubmit}>
          <Typography.Title level={4}>
            Promjena lozinke
          </Typography.Title>
          <Margin margin="0 0 10px">
            <Input.Password
              placeholder="Trenutna lozinka"
              name="oldPassword"
              onChange={formik.handleChange}
              value={formik.values.oldPassword}
            />
            <ErrorMessage name="oldPassword" />
          </Margin>
          <Margin margin="0 0 10px">
            <Input.Password
              placeholder="Nova lozinka"
              name="newPassword"
              onChange={formik.handleChange}
              value={formik.values.newPassword}
            />
            <ErrorMessage name="newPassword" />
          </Margin>
          <Margin margin="0 0 20px">
            <Input.Password
              placeholder="Ponovljena lozinka"
              name="confirmPassword"
              onChange={formik.handleChange}
              value={formik.values.confirmPassword}
            />
            <ErrorMessage name="confirmPassword" />
          </Margin>
          <Button
            type="primary"
            htmlType="submit"
            block
            loading={formik.isSubmitting}
            disabled={!formik.dirty}
          >
            Spremi
          </Button>
        </form>
      )}
    </Formik>
  );
}
