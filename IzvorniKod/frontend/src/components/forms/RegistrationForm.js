import React from 'react';
import { Input, Button, Typography } from 'antd';

import { Margin } from '../core/Margin';
import { Formik } from 'formik';

import { object, string, ref } from 'yup';
import { ErrorMessage } from '../core/ErrorMessage';

const validationSchema = object().shape({
  username: string().required('Morate unijeti korisničko ime'),
  password: string().required('Morate unijeti lozinku'),
  confirmPassword: string()
    .oneOf([ref('password'), ''], 'Lozinke se moraju podudarati')
    .required('Morate ponoviti lozinku'),
  payPalMail: string().required('Morate unijeti e-mail').email('Unesite ispravni e-mail'),
});

const initialValues = {
  firstname: '',
  lastname: '',
  username: '',
  password: '',
  confirmPassword: '',
  payPalMail: '',
};

export function RegistrationForm(props) {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={props.onSubmit}
      validationSchema={validationSchema}
    >
      {(formik) => (
        <form onSubmit={formik.handleSubmit}>
          <Typography.Title level={2}>
            Registracija
          </Typography.Title>
          <Margin margin="0 0 10px">
            <Input
              placeholder="Ime"
              name="firstname"
              onChange={formik.handleChange}
              value={formik.values.firstname}
            />
          </Margin>
            <Margin margin="0 0 25px">
            <Input
              placeholder="Prezime"
              name="lastname"
              onChange={formik.handleChange}
              value={formik.values.lastname}
            />
          </Margin>
          <Margin margin="0 0 10px">
            <Input
              placeholder="Korisničko ime"
              name="username"
              onChange={formik.handleChange}
              value={formik.values.username}
            />
            <ErrorMessage name="username" />
          </Margin>
          <Margin margin="0 0 25px">
            <Input
              placeholder="E-mail"
              name="payPalMail"
              onChange={formik.handleChange}
              value={formik.values.payPalMail}
            />
            <ErrorMessage name="payPalMail" />
          </Margin>
          <Margin margin="0 0 10px">
            <Input.Password
              placeholder="Lozinka"
              name="password"
              onChange={formik.handleChange}
              value={formik.values.password}
            />
            <ErrorMessage name="password" />
          </Margin>
          <Margin margin="0 0 40px">
            <Input.Password
              placeholder="Ponovljena lozinka"
              name="confirmPassword"
              onChange={formik.handleChange}
              value={formik.values.confirmPassword}
            />
            <ErrorMessage name="confirmPassword" />
          </Margin>
          <Button
            type="primary"
            htmlType="submit"
            block
            loading={formik.isSubmitting}
          >
            Stvori račun
          </Button>
        </form>
      )}
    </Formik>
  );
}
