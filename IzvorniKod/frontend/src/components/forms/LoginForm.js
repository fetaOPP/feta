import { Button, Input, Typography } from 'antd';
import { Formik } from 'formik';
import React from 'react';
import { object, string } from 'yup';

import { Margin } from '../core/Margin';
import { ErrorMessage } from '../core/ErrorMessage';

const initialValues = {
  username: '',
  password: '',
};

const validationSchema = object().shape({
  username: string().required('Morate unijeti korisničko ime'),
  password: string().required('Morate unijeti lozinku'),
})

export function LoginForm(props) {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
    >
      {(formik) => (
        <form onSubmit={formik.handleSubmit}>
          <Typography.Title level={2}>
            Prijava
          </Typography.Title>

          <Margin margin="0 0 10px">
            <Input
              placeholder="Korisničko ime"
              name="username"
              onChange={formik.handleChange}
              value={formik.values.username}
            />
            <ErrorMessage name="username" />
          </Margin>
          <Margin margin="0 0 40px">
            <Input.Password
              placeholder="Lozinka"
              name="password"
              onChange={formik.handleChange}
              value={formik.values.password}
            />
            <ErrorMessage name="password" />
          </Margin>

          <Button
            type="primary"
            htmlType="submit"
            loading={formik.isSubmitting}
            block
          >
            Prijava
          </Button>
        </form>
      )}
    </Formik>
  );
}
