import { Input, Button, Typography} from 'antd';
import { Formik } from 'formik';
import React from 'react';
import { object, string } from 'yup';

import { ErrorMessage } from '../core/ErrorMessage';
import { Margin } from '../core/Margin';

const validationSchema = object().shape({
  eMail: string().email('Unesite ispravni e-mail'),
});

const initialValues = {
  username: '',
  eMail: '',
};

export function UpdateUserInfoForm(props) {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={props.onSubmit}
      validationSchema={validationSchema}
    >
      {(formik) => (
        <form onSubmit={formik.handleSubmit}>
          <Typography.Title level={4}>
            Promjena korisničkih podataka
          </Typography.Title>
          <Margin margin="0 0 10px">
            <Input
              placeholder="Novo korisničko ime"
              name="username"
              onChange={formik.handleChange}
              value={formik.values.username}
            />
            <ErrorMessage name="username" />
          </Margin>
          <Margin margin="0 0 20px">
            <Input
              placeholder="Novi e-mail"
              name="eMail"
              onChange={formik.handleChange}
              value={formik.values.eMail}
            />
            <ErrorMessage name="eMail" />
          </Margin>
          <Button
            type="primary"
            htmlType="submit"
            block
            loading={formik.isSubmitting}
            disabled={!formik.dirty}
          >
            Spremi
          </Button>
        </form>
      )}
    </Formik>
  );
}
