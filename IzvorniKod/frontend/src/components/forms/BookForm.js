import React from 'react';

import { Margin } from '../core/Margin';
import { ArtistSelect } from '../core/ArtistSelect';

export function BookForm({ formik }) {
  return (
    <div>  
      <Margin margin="0 0 10px">
        <ArtistSelect
          name="authors"
          placeholder="Autori"
          formik={formik}
        />
      </Margin>
    </div>
  );
}
