import { Modal, Input, Typography } from 'antd';
import React from 'react'

import paypal from '../../static/paypal.png';

import { Panel } from '../core/Panel';
import { Margin } from '../core/Margin';
import { calculateRentPrice } from '../../services/product';

export function PayPalForm({ user, visible, onCancel, purchasePrice, interval, onPurchase }) {
  return (  
    <Modal
      okText="POTVRDI PLAĆANJE"
      cancelText="ODUSTANI"
      visible={visible}
      destroyOnClose
      centered
      closable={false}
      onOk={onPurchase}
      onCancel={onCancel}
      width={320}
    >
      <Panel center>
        <Margin margin="24px 0 24px">
          <img
            src={paypal}
            style={{ maxWidth: '50%'}}
            alt="paypal"
          />
        </Margin>
        <Typography>
          Potvrdite plaćanje od {calculateRentPrice(purchasePrice, interval)} HRK
        </Typography>
        <Margin margin="24px 0 64px">
          <Input
            placeholder="PayPal e-mail"
            name="payPalMail"
            defaultValue={user && user.payPalMail}
          />
        </Margin>
      </Panel>
    </Modal>
  );
}