import { Input } from 'antd';
import React from 'react'

import { Margin } from '../core/Margin';

export function MagazineForm({ formik }) {
  return (
    <div>                   
      <Margin margin="0 0 10px">                
        <Input
          name="issueNumber"
          placeholder="Broj izdanja"
          value={formik.values.issueNumber}
          onChange={formik.handleChange}
        />
      </Margin>
      
      <Margin margin="0 0 10px">                
        <Input
          name="monthOfIssue"
          placeholder="Mjesec izdanja"
          value={formik.values.monthOfIssue}
          onChange={formik.handleChange}
        />
      </Margin>
    </div>
  );
}
