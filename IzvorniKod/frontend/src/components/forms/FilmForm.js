import React from 'react'

import { Margin } from '../core/Margin';
import { ArtistSelect } from '../core/ArtistSelect';

export function FilmForm({ formik }) {
  return (
    <div>
      <Margin margin="0 0 10px">
        <ArtistSelect
          name="directors"
          placeholder="Redatelji"
          formik={formik}
        />
      </Margin>
        
      <Margin margin="0 0 10px">
        <ArtistSelect
          name="producers"
          placeholder="Producenti"
          formik={formik}
        />
      </Margin>
    </div>
  );
}
