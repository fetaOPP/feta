import { Upload, Icon, Button, Radio, Typography, Input, Checkbox } from 'antd';
import { useFormik } from 'formik';
import { inject, observer } from 'mobx-react';
import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';

import { PublisherSelect } from '../core/PublisherSelect';
import { Panel } from '../core/Panel'
import { Margin } from '../core/Margin'
import { ItemCategory } from '../../enums/ItemCategory';
import { FilmForm } from './FilmForm';
import { BookForm } from './BookForm';
import { MagazineForm } from './MagazineForm';
import { SongForm } from './SongForm';
import { uploadThumbnail, uploadFile } from '../../services/api';

function categoryForm(category) {
  switch (category) {
    case ItemCategory.Film: return FilmForm;
    case ItemCategory.Book: return BookForm;
    case ItemCategory.Magazine: return MagazineForm;
    case ItemCategory.Song: return SongForm;
    default: return null;
  }
}

function formatInitialValues(item) {
  if (item) {
    return {
      id: item.id,
      itemType: item.type,
      title: item.title,
      yearOfIssue: item.yearOfIssue,
      description: item.description,
      purchasePrice: item.purchasePrice,
      available: item.available,
      publishersIds: (item.publishers || []).map(p => p.id),
      directorsIds: (item.directors || []).map(p => p.id),
      producersIds: (item.producers || []).map(p => p.id),
      authorsIds: (item.authors || []).map(p => p.id),
      artistsIds: (item.artists || []).map(p => p.id),
      songwritersIds: (item.songwriters || []).map(p => p.id),
    };
  } else {
    return {
      itemType: 'film',
    }
  }
}

function ItemFormC({ item, title, onSubmit, appStore }) {
  const formik = useFormik({
    onSubmit,
    initialValues: formatInitialValues(item),
  });

  const handleTypeChange = (event) => {
    formik.resetForm();
    formik.setFieldValue('itemType', event.target.value);
  }

  const handleAvailabilityChange = (event) => {
    formik.setFieldValue('available', event.target.checked);
  };

  const CategoryForm = categoryForm(formik.values.itemType);

  return (
    <Panel>
      <Typography.Title level={2}>
        {title}
      </Typography.Title> 

      <Panel
        border="2px solid #1890ff"
        borderRadius="7px"
        padding="24px"
      >
        {!item ? (
          <Margin margin="0 0 24px">
            <Typography.Title level={4}>
              Kategorija
            </Typography.Title>
            <Radio.Group
              onChange={handleTypeChange}
              value={formik.values.itemType}
              buttonStyle="solid"
            >
              <Radio.Button value={ItemCategory.Film}>Film</Radio.Button>
              <Radio.Button value={ItemCategory.Book}>Knjiga</Radio.Button>
              <Radio.Button value={ItemCategory.Magazine}>Časopis</Radio.Button>
              <Radio.Button value={ItemCategory.Song}>Glazba</Radio.Button>
            </Radio.Group>
          </Margin>
        ) : null}

        <Margin margin="0 0 10px">
          <Input
            placeholder="Naziv"
            name="title"
            onChange={formik.handleChange}
            value={formik.values.title}
          />              
        </Margin>

        <Margin margin="0 0 10px">
          <Input
            placeholder="Godina izdanja"
            name="yearOfIssue"
            onChange={formik.handleChange}
            value={formik.values.yearOfIssue}
          />
        </Margin>
            
        <CategoryForm formik={formik} />

        <Margin margin="0 0 10px">
          <PublisherSelect
            name="publishers"
            placeholder="Izdavači"
            formik={formik}
          />
        </Margin>

        <Margin margin="0 0 10px">
          <Input
            placeholder="Cijena (HRK)"
            name="purchasePrice"
            value={formik.values.purchasePrice}
            onChange={formik.handleChange}
            disabled={!appStore.user.isAdmin}
          />
        </Margin>

        <Input.TextArea
          rows={4} 
          placeholder="Kratki opis"
          name="description"
          value={formik.values.description}
          onChange={formik.handleChange}
        />

        {!item ? (
          <Fragment>
            <Margin margin="24px 0 12px">
              <Upload
                listType="picture-card"
                action={uploadThumbnail}
                method="PUT"
                headers={{
                  'Content-Type': 'image/png',
                }}
              >
                Odaberi sličicu
              </Upload>
            </Margin>

            <Margin margin='0 0 24px'>
              <Upload
                action={uploadFile}
                method="PUT"
                headers={{
                  'Content-Type': 'image/png',
                }}
              >
                <Button>
                <Icon type="upload" /> Odaberi datoteku
                </Button>
              </Upload>
            </Margin>
          </Fragment>
        ) : null}

        {appStore.user.isAdmin ? (
          <Margin margin="12px 0 24px">
            <Checkbox
              name="available"
              checked={formik.values.available}
              onChange={handleAvailabilityChange}
            >
              Javno dostupno
            </Checkbox>
          </Margin>
        ) : null}

        <div>
          <Button
            type="primary"
            onClick={formik.handleSubmit}
          >
            Spremi
          </Button>&nbsp;&nbsp;&nbsp;
          <Link to="/admin?category=content">
            <Button>Odustani</Button>
          </Link>
        </div>
      </Panel>
    </Panel>
  );
}

export const ItemForm = inject('appStore')(observer(ItemFormC));
