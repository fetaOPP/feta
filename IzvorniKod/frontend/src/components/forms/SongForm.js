import React from 'react';

import { Margin } from '../core/Margin';
import { ArtistSelect } from '../core/ArtistSelect';

export function SongForm({ formik }) {
  return (
    <div>  
      <Margin margin="0 0 10px">
        <ArtistSelect
          name="artists"
          placeholder="Autori"
          formik={formik}
        />
      </Margin>

      <Margin margin="0 0 10px">
        <ArtistSelect
          name="producers"
          placeholder="Producenti"
          formik={formik}
        />
      </Margin>

      <Margin margin="0 0 10px">
        <ArtistSelect
          name="songwriters"
          placeholder="Tekstopisci"
          formik={formik}
        />
      </Margin>
    </div>
  );
}
