import { Typography } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

export function ProductCarouselItem({ product }) {
  return (
    <Link
      to={`/products/${product && product.id}`}
    >
      <div
        style={{
          width: '100%',
          paddingTop: '100%',
          backgroundColor: product ? 'transparent' : 'lightgray',
          backgroundImage: product && `url('${product.iconUrl}')`,
          backgroundSize: 'cover',
          backgroundPosition: 'center center',
          marginBottom: '5px',
        }}
      />
      <Typography.Title
        level={4}
      >
        {product ? product.title : '~ ~ ~'}
      </Typography.Title>
    </Link>
  );
}
