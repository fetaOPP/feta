import { Typography } from 'antd';
import React, { Fragment } from 'react';

import { ProductCarouselItem } from './ProductCarouselItem';

export function ProductCarousel({ title, products = [], loading }) {
  return (
    <div>
      <Typography.Title level={3}>
        {title}
      </Typography.Title>
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: 'repeat(5, 1fr)',
          columnGap: '30px',
        }}
      >
        {loading ? (
          <Fragment>
            {[1,2,3,4,5].map((key) => (
              <ProductCarouselItem
                key={key}
              />
            ))}
          </Fragment>
        ) : products.map((product) => (
          <ProductCarouselItem
            key={product.title}
            product={product}
          />
        ))}
      </div>
    </div>
  );
}
