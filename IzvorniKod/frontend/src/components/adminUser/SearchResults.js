import { Pagination, Empty, Icon, Typography } from 'antd';
import React, { Fragment } from 'react';

import { SearchResultItem } from '../search/SearchResultItem';
import { useQuery } from '../../utils/useQuery';

export function SearchResults({ products }) {
  const [params, updateParams] = useQuery();

  const page = Number(params.get('page') || 1);

  const handlePageChange = (page) => {
    updateParams('page', page);
  }

  return (products.length > 0 ? (
    <Fragment>
      {products.map((product) => (
        <SearchResultItem
          key={product.title}
          product={product}
          smallImage
          hideBuyButton
          footer={(
            <div>
              <Icon type="clock-circle" />&nbsp;&nbsp;
              <Typography.Text>
                Posudba ističe za N dana
              </Typography.Text>
            </div>
          )}
        />
      ))}
      <Pagination
        total={products.length}
        pageSize={5}
        current={page}
        onChange={handlePageChange}
        style={{ textAlign: 'center' }}
      />
    </Fragment>
  ) : (
    <Empty
      image={Empty.PRESENTED_IMAGE_DEFAULT}
      description={`Nažalost za ovaj pojam nismo našli nijedan rezultat.\r\nPokušajte promijeniti pojam pretraživanja ili filtere.`}
      style={{ whiteSpace: 'pre-line' }}
    />
  ));
}
