import React from 'react';
import { Row, Col, Checkbox } from 'antd';

import { useQuery } from '../../utils/useQuery';
import { SearchBox } from '../SearchBox';
import { useArrayParams } from '../../utils/useArrayParams';

export function SearchFilters() {
  const [params, updateParams] = useQuery();
  const [types, toggleType] = useArrayParams('type');

  const handleCategoryUpdate = (event) => {
    const type = event.target.value;
    toggleType(type);
  }

  const handleSearch = (newQuery) => {
    updateParams('query', newQuery);
  }

  return (
    <Row type="flex" justify="space-between">
      <Col>
        <Checkbox
          value="song"
          checked={types.includes('song')}
          onChange={handleCategoryUpdate}
        >
          Glazba
        </Checkbox>
        <Checkbox
          value="film"
          checked={types.includes('film')}
          onChange={handleCategoryUpdate}
        >
          Film
        </Checkbox>
        <Checkbox
          value="magazine"
          checked={types.includes('magazine')}
          onChange={handleCategoryUpdate}
        >
          Časopis
        </Checkbox>
        <Checkbox
          value="book"
          checked={types.includes('book')}
          onChange={handleCategoryUpdate}
        >
          Knjiga
        </Checkbox>
      </Col>
      <Col>
        <SearchBox
          size="default"
          placeholder="Pretraži sadržaj"
          defaultValue={params.get('query')}
          onSearch={handleSearch}
        />
      </Col>
    </Row>
  )
}
