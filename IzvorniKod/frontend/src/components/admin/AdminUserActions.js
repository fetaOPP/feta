import { Button, Popconfirm, Modal, Select, Typography } from 'antd';
import { observer, inject } from 'mobx-react';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { Margin } from '../core/Margin';
import { updateUserRole, deleteUser } from '../../services/user';
import { notify } from '../../utils/notifications';

function AdminUserActionsC({ user, onChange, appStore }) {
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [role, setRole] = useState(user.role);

  const closeModal = () => setEditModalOpen(false);
  const openModal = () => setEditModalOpen(true);

  const handleChangeRole = async () => {
    await updateUserRole(user, role);
    setEditModalOpen(false);

    notify('success', 'Uspješno ste promijenili korisnikovu ulogu!');
    onChange();
  };

  const handleDeleteUser = async () => {
    await deleteUser(user);
    setEditModalOpen(false);

    notify('success', 'Uspješno ste izbrisali korisnika!');
    onChange();
  }

  const disableChange = Number(user.id) === appStore.user.id;

  return (
    <div>
      <Button.Group>
        <Button>
          <Link to={`/admin/user/${user.id}`}>
            Pregled  
          </Link>
        </Button>
        <Button onClick={openModal} disabled={disableChange}>Uredi</Button>
        <Popconfirm
          title="Jeste li sigurni da želite izbrisati ovog korisnika?"
          okText="Izbriši"
          cancelText="Odustani"
          okType="danger"
          onConfirm={handleDeleteUser}
          disabled={disableChange}
        >
          <Button
            disabled={disableChange}
          >
            Izbriši
          </Button>
        </Popconfirm>
      </Button.Group>
      <Modal
        visible={editModalOpen}
        destroyOnClose
        onCancel={closeModal}
        onOk={handleChangeRole}
        title="Uredi korisnika"
      >
        <Margin margin="0 0 5px">
          <Typography.Text strong>
            Uloga
          </Typography.Text>
        </Margin>
        <Select
          value={role}
          onChange={setRole}
          style={{
            width: '300px',
          }}
        >
          <Select.Option value="USER">Korisnik</Select.Option>
          <Select.Option value="COORDINATOR">Koordinator</Select.Option>
          <Select.Option value="ADMIN">Administrator</Select.Option>
        </Select>
      </Modal>
    </div>
  );
}

export const AdminUserActions = inject('appStore')(observer(AdminUserActionsC));
