import { Table, Spin } from 'antd';
import React from 'react';

import { Margin } from '../core/Margin';
import { AdminUserActions } from './AdminUserActions';
import { useUserSearch } from '../../services/user';
import { observer } from 'mobx-react';

let fetchRecords;

const columns = [
  {
    title: 'Korisničko ime',
    dataIndex: 'username',
    key: 'username',
  },
  {
    title: 'E-mail',
    dataIndex: 'payPalMail',
    key: 'payPalMail',
  },
  {
    title: 'Uloga',
    dataIndex: 'role',
    key: 'role',
  },
  {
    title: 'Akcije',
    dataIndex: '',
    key: '',
    align: 'right',
    render: (_, record) => <AdminUserActions user={record} onChange={fetchRecords} />
  }
];

function AdminUsersC() {
  const {items, loading, params, updateParams, totalCount, onFetch} = useUserSearch({
    size: 8,
    include: '',
  });

  fetchRecords = onFetch;

  const page = Number(params.get('page') || 1);

  const handlePageChange = (page) => {
    updateParams('page', page);
  }

  return (
    <div>
      <Margin margin="32px 0 0">
        {loading ? (
          <div style={{ textAlign: 'center' }}>
            <Spin size="large" />   
          </div>
        ) : (
          <Table
            columns={columns}
            dataSource={items}
            pagination={{
              total: totalCount,
              pageSize: Number(params.get('size')),
              current: page,
              onChange: handlePageChange,
              style: { textAlign: 'center' },
            }}
            rowKey="id"
          />
        )}
      </Margin>
    </div>
  );
}

export const AdminUsers = observer(AdminUsersC);
