import React from 'react';
import { useHistory } from 'react-router-dom';

import { Panel } from '../core/Panel';
import { SearchBox } from '../SearchBox';
import logo from '../../static/logo.png';

export function HomeSearch() {
  const history = useHistory();

  function handleSearch(value) {
    history.push(`/search?query=${value}`);
  }

  return (
    <Panel
      maxWidth="500px"
      center
    >
      <img
        alt=""
        src={logo}
        style={{
          width: '200px',
          marginBottom: '40px',
        }}
      />
      <SearchBox
        onSearch={handleSearch}
      />
    </Panel>
  );
}
