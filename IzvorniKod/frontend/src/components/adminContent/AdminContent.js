import { Row, Col, Button, Table, Icon, Spin, Tooltip } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

import { Margin } from '../core/Margin';
import { useSearch } from '../../utils/useSearch';

const itemIcon = (type) => {
  switch (type) {
    case 'book': return 'book';
    case 'film': return 'play-square';
    case 'song': return 'customer-service';
    case 'magazine': return 'read';
    default: return 'line';
  }
}

const columns = [
  {
    key: 'productIcon',
    render: (_, record) => (
      <Icon type={itemIcon(record.type)} style={{ fontSize: '20px' }} />
    ),
    width: '50px',
  },
  {
    title: 'Naziv',
    key: 'title',
    render: (_, record) => record.available ? (
      <Link
        to={`/products/${record.id}`}
      >
        {record.title}
      </Link>
    ) : record.title,
  },
  {
    title: 'Godina',
    dataIndex: 'yearOfIssue',
    key: 'yearOfIssue',
  },
  {
    title: 'Info',
    dataIndex: 'info',
    key: 'info',
  },
  {
    title: 'Cijena',
    dataIndex: 'formattedPurchasePrice',
    key: 'formattedPurchasePrice',
  },
  {
    key: 'editAction',
    align: 'right',
    render: (_, record) => (
      <div>
        {!record.available && (
          <Tooltip title="Ovaj proizvod nije javno dostupan jer mu nedostaje cijena.">
            <Icon type="exclamation-circle" theme="filled" />
          </Tooltip>
        )}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <Button>
          <Link to={`/admin/content/${record.id}`}>
            Uredi
          </Link>
        </Button>
      </div>
    ),
  },
];

export function AdminContent() {
  const {items, loading, params, updateParams, totalCount} = useSearch({
    size: 8,
    include: 'publishers,authors,artists,songwriters,producers,directors',
  });

  const page = Number(params.get('page') || 1);

  const handlePageChange = (page) => {
    updateParams('page', page);
  }

  return (
    <div>
      <Margin margin="0 0 48px">
        <Row type="flex" justify="end">
          <Col>
            <Button type="primary">
              <Link to="/admin/content/new">
                Novi proizvod
              </Link>
            </Button>
          </Col>
        </Row>
      </Margin>
      {loading ? (
        <div style={{ textAlign: 'center' }}>
          <Spin size="large" />   
        </div>
      ) : (
        <Table
          columns={columns}
          dataSource={items}
          pagination={{
            total: totalCount,
            pageSize: Number(params.get('size')),
            current: page,
            onChange: handlePageChange,
            style: { textAlign: 'center' },
          }}
          rowKey="id"
        />
      )}
    </div>
  )
}
