import React from 'react';

import { useCarousel } from '../../services/pages/home';
import { Margin } from '../core/Margin';
import { ProductCarousel } from '../ProductCarousel/ProductCarousel';

export function ProductSuggestions({ type }) {
  const [items, itemsLoading] = useCarousel(type);

  return (
    <Margin margin="0 0 40px">
      <ProductCarousel
        title="Iz iste kategorije"
        products={items}
        loading={itemsLoading}
      />
    </Margin>
  )
}
