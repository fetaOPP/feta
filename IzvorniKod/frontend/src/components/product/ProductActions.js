import { Modal } from 'antd';
import React, { Component } from 'react';

import { ProductTransactionActions } from './ProductTransactionActions';
import { ProductBoughtActions } from './ProductBoughtActions';
import { ProductRentedActions } from './ProductRentedActions';
import { PayPalForm } from '../forms/PayPalForm';
import { createPurchase } from '../../services/purchase';
import { fetchSession } from '../../services/session';
import { notify } from '../../utils/notifications';
import { inject, observer } from 'mobx-react';

class ProductActionsC extends Component {
  constructor(props) {
    super(props);

    const { appStore, product } = props;

    const transaction = appStore.user && appStore.user.transactionForItem(product.id);

    this.state = {
      bought: transaction ? transaction.isBought : false,
      rented: transaction ? transaction.isRented : false,
      playerOpen: false,
      paypalOpen: false,
      interval: '',
    };

    this.handleBuy = this.handleBuy.bind(this);
    this.handleRent = this.handleRent.bind(this);
    this.handleOpenPlayer = this.handleOpenPlayer.bind(this);
    this.handleClosePlayer = this.handleClosePlayer.bind(this);
    this.handleClosePaypal = this.handleClosePaypal.bind(this);
    this.handlePurchase = this.handlePurchase.bind(this);
  }

  handleBuy() {
    this.setState({ paypalOpen: true, interval: '' });
  }

  handleRent(interval) {
    this.setState({ paypalOpen: true, interval });
  }

  handleClosePaypal() {
    this.setState({ paypalOpen: false });
  }

  async handlePurchase() {
    try {
      await createPurchase(this.props.product.id, this.state.interval);
      await fetchSession(this.props.appStore);
    } catch {
      notify('error', 'Greška kod izvršavanja plaćanja', 'Molimo pokušajte ponovno');
      this.setState({ paypalOpen: false });

      return;
    }

    if (this.state.interval) {
      notify('success', 'Uspješno ste posudili proizvod.');
      this.setState({ paypalOpen: false, rented: true });
    } else {
      notify('success', 'Uspješno ste kupili proizvod.');
      this.setState({ paypalOpen: false, bought: true });
    }
  }

  get productActions() {
    const { product } = this.props;

    if (this.state.bought) {
      return (
        <ProductBoughtActions
          product={product}
          onOpenPlayer={this.handleOpenPlayer}
        />
      );
    } else if (this.state.rented) {
      return (
        <ProductRentedActions
          product={product}
          onBuy={this.handleBuy}
          onOpenPlayer={this.handleOpenPlayer}
        />
      );
    } else {
      return (
        <ProductTransactionActions
          product={product}
          onBuy={this.handleBuy}
          onRent={this.handleRent}
        />
      )
    } 
  }

  handleOpenPlayer() {
    this.setState({ playerOpen: true });
  }

  handleClosePlayer() {
    this.setState({ playerOpen: false });
  }

  render() {
    const {
      product,
      appStore,
    } = this.props;

    return (
      <div>
        <Modal
          visible={this.state.playerOpen}
          centered
          footer={null}
          closable={false}
          width="60%"
          bodyStyle={{ padding: '0' }}
          onCancel={this.handleClosePlayer}
          destroyOnClose
        >
          <video
            src={product.playerSrc}
            width="100%"
            controls
          />
        </Modal>
        <PayPalForm
          visible={this.state.paypalOpen}
          onCancel={this.handleClosePaypal}
          purchasePrice={product.purchasePrice}
          interval={this.state.interval}
          onPurchase={this.handlePurchase}
          user={appStore.user}
        />
        {this.productActions}
      </div>
    )
  }
}

export const ProductActions = inject('appStore')(observer(ProductActionsC));
