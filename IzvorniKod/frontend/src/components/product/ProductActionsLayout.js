import { Row, Col } from 'antd';
import React from 'react';

import { MaxWidth } from '../core/MaxWidth';

export function ProductActionsLayout({ leftAction, rightAction }) {
  return (
    <MaxWidth maxWidth="360px">
      <Row gutter={16}>
        <Col span={12}>
          {leftAction}
        </Col>
        <Col span={12}>
          {rightAction}
        </Col>
      </Row>
    </MaxWidth>
  )
}
