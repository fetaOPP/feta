import { Button, Typography } from 'antd';
import React, { Fragment } from 'react';

import { ProductActionsLayout } from './ProductActionsLayout';
import { Margin } from '../core/Margin';

export function ProductBoughtActions({ product, onOpenPlayer }) {
  return (
    <Fragment>
      <ProductActionsLayout
        leftAction={(
          <a
            href={product.fileUrl}
            download
            target="_blank"
            rel="noopener noreferrer"
          >
            <Button
              size="large"
              type="primary"
              block
            >
              Otvori
            </Button>
          </a>
        )}
        rightAction={(
          <a
            href={product.fileUrl}
            download
            target="_blank"
            rel="noopener noreferrer"
          >
            <Button
              size="large"
              block
            >
              Skini
            </Button>
          </a>
        )}
      />
      <Margin margin="16px 0 0">
        <Typography.Text>
          Kupili ste ovaj proizvod i možete ga otvoriti ili skinuti.
        </Typography.Text>
      </Margin>
    </Fragment>
  );
}
