import { Button, Select, Typography } from 'antd';
import { inject, observer, PropTypes } from 'mobx-react';
import React, { Fragment, useState } from 'react';
import { ProductActionsLayout } from './ProductActionsLayout';
import { Margin } from '../core/Margin';
import { calculateRentPrice } from '../../services/product';

function ProductTransactionActionsC({ product, onBuy, onRent, appStore }) {
  const [rentInterval, setRentInterval] = useState('24_hours');

  const handleRentIntervalChange = (interval) => {
    setRentInterval(interval);
  }

  const handleRent = () => onRent(rentInterval);

  return (
    <Fragment>
      <ProductActionsLayout
        leftAction={(
          <Button
            size="large"
            type="primary"
            block
            onClick={onBuy}
            disabled={!appStore.isLoggedIn}
          >
            Kupi za {product.purchasePrice} HRK
          </Button>
        )}
        rightAction={(
          <Fragment>
            <Button
              size="large"
              block
              onClick={handleRent}
              disabled={!appStore.isLoggedIn}
            >
              Posudi za {calculateRentPrice(product.purchasePrice, rentInterval)} HRK
            </Button>
            <Margin margin="8px 0 0">
              <Select
                size="large"
                style={{ width: '100%' }}
                defaultValue="24_hours"
                allowClear={false}
                onChange={handleRentIntervalChange}
              >
                <Select.Option value="24_hours">24 sata</Select.Option>
                <Select.Option value="48_hours">48 sati</Select.Option>
                <Select.Option value="1_week">1 tjedan</Select.Option>
                <Select.Option value="1_month">1 mjesec</Select.Option>
              </Select>
            </Margin>
          </Fragment>
        )}
      />
      {!appStore.isLoggedIn && (
        <Margin margin="16px 0 0">
          <Typography.Text>
            Da bi mogli kupiti ili posuditi ovaj proizvod, prvo se morate prijaviti.
          </Typography.Text>
        </Margin>
      )}
    </Fragment>
  );
}

export const ProductTransactionActions = inject('appStore')(observer(ProductTransactionActionsC));
