import React, { Fragment } from 'react';
import { ProductActionsLayout } from './ProductActionsLayout';
import { Button, Typography } from 'antd';
import { Margin } from '../core/Margin';

export function ProductRentedActions({ product, onBuy, onOpenPlayer }) {
  return (
    <Fragment>
      <ProductActionsLayout
        leftAction={(
          <a
            href={product.fileUrl}
            download
            target="_blank"
            rel="noopener noreferrer"
          >
            <Button
              size="large"
              type="primary"
              block
            >
              Otvori
            </Button>
          </a>
        )}
        rightAction={(
          <Button
            size="large"
            block
            onClick={onBuy}
          >
            Kupi za {product.purchasePrice} HRK
          </Button>
        )}
      />
      <Margin margin="16px 0 0">
        <Typography.Text>
          Posudili ste ovaj proizvod i možete ga otvoriti ili kupiti.
        </Typography.Text>
      </Margin>
    </Fragment>
  );
}