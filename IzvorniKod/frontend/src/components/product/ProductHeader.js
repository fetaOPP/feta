import React, { Fragment } from 'react';
import { Typography } from 'antd';
import { Margin } from '../core/Margin';
import { ProductActions } from './ProductActions';

export function ProductHeader({ product }) {
  return (
    <Fragment>
      <Typography.Title level={2}>
        {product.title}
      </Typography.Title>
      <Margin margin="-32px 0 32px">
        <Typography.Title level={3}>
          {product.year}
        </Typography.Title>
      </Margin>
      <Margin margin="0 0 24px">
        <Typography.Text strong style={{ fontSize: '20px', }}>
          {product.description}
        </Typography.Text>
      </Margin>
        <Typography.Text strong style={{ fontSize: '20px', display: 'block', whiteSpace: 'pre' }}>
          {product.formattedInfo}
        </Typography.Text>
      <Margin margin="40px 0 0">
        <ProductActions
          product={product}
        />
      </Margin>
    </Fragment>
  );
}
