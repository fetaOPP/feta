import React from 'react';

export function ProductImage({ src, style = {} }) {
  return (
    <img
      src={src}
      alt="product-poster"
      style={{
        width: '100%',
        boxShadow: '0 0 20px 0 rgba(0,0,0,0.3)',
        ...style
      }}
    />
  )
}
