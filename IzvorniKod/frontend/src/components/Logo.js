import React from 'react';

import logotipSrc from '../static/logotip.png';

export function Logo() {
  return (
    <img
      src={logotipSrc}
      alt="Mediateka logo"
      style={{
        width: '32px',
      }}
    />
  );
}